//
//  NeuralNetwork.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/05/07.
//
//

#pragma once
#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"
#include "cinder/Rand.h"

#include "Resources.h"
#include <boost/lexical_cast.hpp>


using namespace ci;
using namespace ci::app;
using namespace std;

class NeuralNetwork {
public:
  NeuralNetwork();
  ~NeuralNetwork();
  void fire();
  void learn( float T );
  
  void input( float (&array)[10] ); // 10 feelings + 1 dominant emotion bias
                                    // 12 hidden nodes
  
  float getOutput();
  float activationFunction( float f );
  
  string getInput( int n );
  string getWeight( int n );
  
private:
  
  float mNetwork[3][10] = {};

  float mInputWeights[10][10] = {};
  float mHiddenWeights[10][1] = {};
  int mInputs = 10;
  int mHidden = 10;
  int mOutput = 1;
  
  int mInputLayer = 0;
  int mHiddenLayer = 1;
  int mOutputLayer = 2;
  
};