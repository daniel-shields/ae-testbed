//
//  ParticleController.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/02/19.
//
//

#pragma once

#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"
#include "cinder/Rand.h"

#include <Box2D/Box2D.h>

#include "Resources.h"
#include "Particle.h"

using namespace ci;
using namespace ci::app;
using namespace std;


class ParticleController {
public:
  ParticleController( b2World &mSandbox );
  ~ParticleController();
  void addParticles( int num );
  void addParticles( Vec2f mousePos );

  void update();
  void center();
  void draw();
  void debugDraw();

  
private:
  b2World     *mWorld;

  vector<Particle*>    mParticles;
  
};