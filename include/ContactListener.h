//
//  ContactListener.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/03/13.
//
//

#pragma once

#include "cinder/app/AppNative.h"
#include "cinder/Rect.h"
#include "cinder/Vector.h"
#include "cinder/Rand.h"

#include <Box2D/Box2D.h>
#include <memory> // smart pointers

#include "Resources.h"
#include "Agent.h"

using namespace ci;
using namespace ci::app;
using namespace std;


class ContactListener : public b2ContactListener {
  
  void BeginContact( b2Contact* contact );
  void EndContact( b2Contact* contact );
  
  // if bool state == true, contact is starting, if == false, contact is ending
  bool handleContact( b2Contact* contact, Agent*& sensor, Agent*& particle, bool state );

};

