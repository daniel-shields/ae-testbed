//
//  Monitor.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/03/24.
//
//

#pragma once
#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"
#include "cinder/Rand.h"
#include "cinder/Text.h"
#include "cinder/gl/Texture.h"
#include "cinder/Vector.h"

#include "PretzelGui.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class Monitor {
public:
  void addMonitor(string text);
  void addDependancyMonitor( float (&array)[11][4] );
  void update();
  void draw();
  void toggle();
  
  Vec2f mPos;
  
  float mOpacity;
  bool  mState;
  
  Monitor();
  ~Monitor();
  
private:
  Pretzel::PretzelGui   *mGui;
  Pretzel::PretzelGui   *mAngerGui;
  Pretzel::PretzelGui   *mSadnessGui;
  Pretzel::PretzelGui   *mFearGui;
  Pretzel::PretzelGui   *mHappinessGui;

  string names[11] = { "Hunger", "Eating", "Smell", "Thirst", "Drinking", "Humidity", "Pain", "Restlessness", "Crowding", "Threat", "Bias" };
};

