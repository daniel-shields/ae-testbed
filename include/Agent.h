//
//  Agent.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/04/15.
//
//

#pragma once
#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"
#include "cinder/Rand.h"
#include "cinder/Xml.h"

#include <Box2D/Box2D.h>

#include "Resources.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class Agent {
public:
  virtual void update() = 0;
  virtual void draw() = 0;
  virtual void drawGui() = 0;
  virtual void toggleInfo() = 0;
  
  virtual b2Body *getBody() = 0;
  virtual agentFixtureType getAgentType() = 0;
  virtual void applyLinearImpulse( b2Vec2 impulse ) = 0;
  virtual b2Vec2 getLinearVelocity() = 0;
  
  virtual void visionAcquiredEnemy( Agent *agent ) = 0;
  virtual void visionLostEnemy( Agent *agent ) = 0;

  virtual void setHero() = 0;
  virtual void unsetHero() = 0;
  virtual void setHealth( float value ) = 0;
  virtual bool getHealth() = 0;
  virtual string getXmlString() = 0;
  virtual XmlTree getXmlVertices() = 0;
  
  virtual ~Agent(){}
};