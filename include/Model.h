//
//  Model.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/05/05.
//
//

#pragma once
#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"
#include "cinder/Rand.h"

#include <Box2D/Box2D.h>
#include <boost/lexical_cast.hpp>

#include "Resources.h"
#include "Monitor.h"
#include "NeuralNetwork.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class Model {
public:
  Model();
  void update();
  void draw();
  void toggle( bool state );
  void info();
  void updateInfo();
  void learn();
  int max_behaviour();
  float get_neural_output( int a );
  
  void  calculate_feelings();
  void  calculate_emotions();
  void  calculate_hormones();

  float emotional_intensity( int n );
  float sensation_intensity( int n );
  float feeling_intensity( int n ); // Ifn
  float hormone_influence( int n ); // Hfn
  float emotional_influence( int n ); // Afn
  float hormone_gain( int n ); // @n
  void calculate_dominant();
  
  void calculate_influence();

  
  void calculate_emotion_based_reinforcement();
  void calculate_sensation_based_reinforcement();
  void select_behaviour(); // Pn
  float probability_of_behaviour_selection( int a );
  
  string get_sensations();
  string get_feelings();
  string get_emotions();
  string get_hormones();
  _entitySubState get_behaviour(){ return behaviour; }
  
  float getEmotion(_entityEmotion emotion);
  float getFeeling(int n);
  float getSensation(int n);
  void  setSensation( int sensation, float value );
  _entityEmotion getDominantEmotion();
  _entitySubState behaviour;
  
private:
  bool drawState = false;
  bool drawInfo = false;
  
  Monitor *mMonitor;
  NeuralNetwork *mNeuralNetwork;
  
  NeuralNetwork *mNeuralActionFWRD; // investigate action
  NeuralNetwork *mNeuralActionBWRD; // escape action
  NeuralNetwork *mNeuralActionXPLR; // explore action
  NeuralNetwork *mNeuralActionDONT; // dont move action
  
  
  _entityEmotion dominantEmotion;

  int behaviouralAction;
  
  int amountOfActions = 3;
  // EMOTIONS
  int mHappiness = 0;
  int mSadness = 1;
  int mFear = 2;
  int mAnger = 3;
  float mEmotions[4];   // emotions[mFear] = x;
  float mEmotionReinforcement[4] = {};   // emotions[mFear] = x;
  float mActionSelection[4] = {};
  
  // FEELINGS
  float mFeelings[10] = {};        // e.g. mFeelings[mHunger] = x;
  float mSensations[10] = {}; // e.g. mSensations[mHunger] = x;
  float mHormones[10] = {};
  float mEmotionalInfluence[10] = {};
  
  float mCoeficient[11][4] = {
    { -0.2f,  0.7f, -0.2f,  0.2f }, // mHunger
    {  0.4f, -0.4f, -0.2f, -0.2f }, // mEating
    {  0.3f,  0.0f, -0.2f,  0.0f }, // mSmell
    { -0.2f,  0.2f, -0.2f,  0.2f }, // mThirst
    {  0.4f, -0.4f, -0.2f,  0.2f }, // mDrinking
    {  0.3f,  0.0f, -0.2f,  0.0f }, // mHumidity
    { -0.3f,  0.0f,  0.7f,  0.1f }, // mPain
    { -0.2f,  0.1f, -0.2f,  0.7f }, // mRestlessness
    {  0.0f,  0.0f,  0.0f,  0.0f }, // mCrowding
    { -0.3f,  0.1f,  0.7f,  0.3f }, // mThreat
    {  0.1f, -0.1f,  0.0f,  0.0f }  // mBias
    // ^ Happiness
    //        ^ Sadness
    //                ^ Fear
    //                      ^ Anger
  };
  
  
  float emotion_activation_threshold = 0.2f; // Emotion Activation Threshold
  float emotion_selection_threshold = 0.2f; // Emotion Selection Threshold
  float hormone_coefficient = 0.9f; // Hormone Coefficient
  float hormone_attack_gain = 0.98f; // Hormone Attack Gain
  float hormone_decay_gain = 0.996f; // Hormone Decay Gain

  // info panel
  float mOpacity;
  float mCounter;
  int mFadeTime;
  gl::Texture mTexture;
  gl::Texture mInfoTexture;
  gl::Texture mSensationsTexture;
  gl::Texture mFeelingsTexture;
  gl::Texture mHormonesTexture;
  
};
