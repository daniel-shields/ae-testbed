#pragma once
#include "cinder/CinderResources.h"

// Box2d Screen to World Conversion
// 1PX = 1M

//#define RES_MY_RES			CINDER_RESOURCE( ../resources/, image_name.png, 128, IMAGE )

#define COLOUR_HAPPY  ColorA( 1.0f, 0.98f, 0.54f, 0.99f )
#define COLOUR_SAD    ColorA( 0.44f, 0.98f, 0.84f, 0.99f )
#define COLOUR_ANGRY  ColorA( 1.0f, 0.12f, 0.10f, 0.99f )
#define COLOUR_FEAR   ColorA( 0.48f, 0.5f, 0.96f, 0.99f )
#define COLOUR_NONE   ColorA( 0.83f, 0.83f, 0.83f, 0.99f )
#define COLOUR_HERO   ColorA( 1.0f, 0.17f, 0.56f, 0.99f )
#define COLOUR_WHITE  ColorA( 1.0f, 1.0f, 1.0f, 0.99f )
#define COLOUR_DEAD   ColorA( 0.13, 0.13, 0.13, 0.99f )

#define COLOUR_ROCK   ColorA( 1.0f, 0.83f, 0.52f, 0.7f )
#define COLOUR_FOOD   ColorA( 0.82f, 0.98f, 0.54f, 0.8f )
#define COLOUR_WATER  ColorA( 0.0f, 0.5f, 0.82f, 0.5f )
#define COLOUR_BG     ColorA( 0.3f, 0.25f, 0.25f )

#define COLOUR_DRAW   ColorA( 0.0f, 0.55f, 0.33f, 0.99f )

enum agentFixtureType {
  _BOUNDARY,
  _FRIEND,
  _PREDATOR,
  _RADAR,
  _SENSOR,
  _TREE,
  _FOOD,
  _WATER,
  _HERBIVORE,
};

enum _entityCategory {
  BOUNDARY         = 0x0001,
  SENSOR           = 0x0001,
  HERBIVORE        = 0x0002,
  PREDATOR         = 0x0002,
  FOOD             = 0x0004,
  WATER            = 0x0008,
  RADAR_SENSOR     = 0x0010,

};

enum _entitySubState {
  MOVE_FWRD = 0x1,
  MOVE_BKWD = 0x2,
  MOVE_XPLR = 0x3,
  MOVE_STOP = 0x4,
};

enum _entityEmotion {
  HAPPINESS = 0x1,
  SADDNESS = 0x2,
  FEAR  = 0x3,
  ANGER  = 0x4,
  NONE = 0x5,
};

enum _entityFeeling {
  HUNGER       = 0x1,
  PAIN         = 0x2,
  RESTLESSNESS = 0x3,
  THIRST       = 0x4,
  EATING       = 0x5,
  SMELL        = 0x6,
  DRINKING     = 0x7,
  PROXIMITY    = 0x8
};

enum _entitySensation {
  mHunger       = 0,  // energy level
  mEating       = 1,  // if eating
  mSmell        = 2,  // count food in area
  mThirst       = 3,  // thirst level
  mDrinking     = 4,  // if drinking
  mHumidity     = 5,  // if near water
  mPain         = 6,  // if touching boundary
  mRestlessness = 7,  // if bored
  mCrowding     = 8,  // count friends in area
  mThreat       = 9,  // enemies in area
  mBias         = 10
};
