//
//  Predator.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/04/15.
//
//

#pragma once
#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"
#include "cinder/Rand.h"

#include <Box2D/Box2D.h>
#include "Agent.h"
#include "Model.h"
#include "Monitor.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class Predator : public Agent  {
public:
  Predator( b2World* mSandbox, b2Vec2 &position );
  ~Predator();
  void update();
  void draw();
  void drawGui();
  void pullToCenter();
  void boids();
  void model();
  void behaviour();
  void toggleInfo();
    bool getHealth(){ return mIsDead; }
  
  string getXmlString();
   XmlTree getXmlVertices();
  
  void applyLinearImpulse( b2Vec2 impulse ){
    body->ApplyLinearImpulse(impulse, body->GetWorldCenter());
  }
  b2Vec2 getLinearVelocity(){ return mLinearVelocity; }
  
  // Sensors
  void visionAcquiredEnemy( Agent *agent );
  void visionLostEnemy( Agent *agent );
  // Contact listener
  void startContact(){ mContact = true; }
  void endContact(){ mContact = false; }
  
  b2Body *getBody(){
    return body;
  }
  
  agentFixtureType getAgentType(){
    return mAgentType;
  }
  
  void setHero(){ mHero = true; }
  void unsetHero(){ mHero = false; mInfo = false; }
  void setHealth( float value ){ mHealth += value;  }
  
  friend class Agent;
  
private:
  b2World *mWorld;
  b2Body  *body;
  vector<Agent*>    mVisible;
  Model *mModel;
  
  b2BodyDef           bodyDef;
  b2PolygonShape      dynamicBox;
  b2FixtureDef        fixtureDef;
  b2RevoluteJointDef  jointDef;
  
  bool    mHero;
  bool    mContact;
  bool    mInfo = false;
  b2Vec2  mPosition;
  b2Vec2  mLinearVelocity;
  float   mAngle;
  float   mCircleRadius;
  int verticesCount;
  uint16 categoryBits = PREDATOR;
  uint16 maskBits = BOUNDARY | PREDATOR | HERBIVORE | FOOD;
  agentFixtureType mAgentType = _PREDATOR;
  
  // BOIDS
  float mZoneRadius = 30.0f * 30.0f * 10.0f;
  float mThreshold = 65.0f;
  float mHighThreshold = 0.75f;
  
  // STATS
  //float mOHunger;
  bool  mIsHungry;
  bool  mIsDead;
  //float mThirst;
  bool  mIsThirsty;
  float mCrowdFactor;
  
  // Physics
  float mMaxForwardSpeed;
  float mMaxBackwardSpeed;
  float mMaxDriveForce;
  float mMaxLateralImpulse;
  float mLastDriveImpulse;
  float mLastLateralFrictionImpulse;
  float mCurrentTraction;
  b2Vec2 pointOfInterest;
  
  float mHealth;
  
};