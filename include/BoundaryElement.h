//
//  BoundaryElement.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/02/18.
//
//

#pragma once

#include "cinder/app/AppNative.h"
#include "cinder/Rect.h"
#include "cinder/Vector.h"
#include "cinder/Rand.h"

#include "cinder/Utilities.h"
#include "cinder/Xml.h"

#include <Box2D/Box2D.h>
#include <ctime>
#include <boost/lexical_cast.hpp>

#include "Resources.h"
#include "AgentFactory.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class BoundaryElement{
public:
    BoundaryElement( b2World &mSandbox, AgentFactory &mFactory );
    ~BoundaryElement();
  
    void createBoundaryElement();
    void draw();
  
    void loadMap( string pathToFile );
    void saveMap();
  
  b2Vec2 getWorldSize();
  
private:
    b2World *mWorld;
    AgentFactory  *mAgentFactory;

    b2ChainShape chain;
    b2BodyDef groundBodyDef;
    b2Vec2 edgeVertices[4];
    vector<b2Vec2*>    mVertices;
  
    int height;
    int width;
  
    int mWorldWidth;
    int mWorldHeight;
    string mWorldName;
    string mWorldOwner;
    bool mWorldLoaded = false;
  
};