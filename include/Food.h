//
//  Food.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/04/15.
//
//

#pragma once
#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"
#include "cinder/Rand.h"

#include <Box2D/Box2D.h>
#include "Agent.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class Food : public Agent  {
public:
  Food( b2World* mSandbox, b2Vec2 &position );
  ~Food(){}

  void update();
  void draw();
  void drawGui(){}
  void pullToCenter();
  void boids();
  void toggleInfo(){}
    bool getHealth(){ return mIsDead; }
  
  void applyLinearImpulse( b2Vec2 impulse ){}
  b2Vec2 getLinearVelocity(){ return mLinearVelocity; }
  
  string getXmlString();
   XmlTree getXmlVertices();
  
  // Sensors
  void visionAcquiredEnemy( Agent *agent ){}
  void visionLostEnemy( Agent *agent ){}
  // Contact listener
  void startContact(){ mContact = true; }
  void endContact(){ mContact = false; }

  b2Body *getBody(){
    return body;
  }
  
  agentFixtureType getAgentType(){
    return mAgentType;
  }
  
  void setHero(){}
  void unsetHero(){}
  
  void setHealth( float value );
  
private:
  b2World *mWorld;
  b2Body  *body;
  
  b2BodyDef           bodyDef;
  b2PolygonShape      dynamicBox;
  b2FixtureDef        fixtureDef;
  b2RevoluteJointDef  jointDef;
  
  bool    mHero;
  bool    mContact;
  b2Vec2  mPosition;
  b2Vec2  mLinearVelocity;
  float   mAngle;
  float   mCircleRadius;
  float   mRadius;

  
  uint16 categoryBits = FOOD;
  uint16 maskBits = BOUNDARY | PREDATOR | HERBIVORE;
  agentFixtureType mAgentType = _FOOD;
  
  // BOIDS
  float mZoneRadius = 30.0f * 30.0f * 10.0f;
  float mThreshold = 65.0f;
  float mHighThreshold = 0.75f;
  
  vector<Agent*>    mVisible;

  // STATS
  float mHunger;
  bool  mIsHungry;
  bool  mIsDead;
  float health = 100;
  
};