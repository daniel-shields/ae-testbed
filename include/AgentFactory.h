//
//  AgentFactory.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/04/15.
//
//

#pragma once
#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"
#include "cinder/Rand.h"
#include "cinder/Xml.h"

#include <Box2D/Box2D.h>

#include "Resources.h"
#include "Agent.h"
#include "Predator.h"
#include "Herbivore.h"
#include "Food.h"
#include "Water.h"
#include "Rock.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class AgentFactory {
public:
  AgentFactory( b2World &mSandbox  ){ mWorld = &mSandbox; }
  ~AgentFactory();
  
  Agent *getAgent( agentFixtureType type, b2Vec2 &position,
                    vector<b2Vec2*> mVertices = vector<b2Vec2*>() ){
    // Factory method
    Agent *agent = NULL;
    
    switch( type ){
      case _HERBIVORE:
        agent = new Herbivore( mWorld, position );
        break;
      case _PREDATOR:
        agent = new Predator( mWorld, position );
        break;
      case _FOOD:
        agent = new Food( mWorld, position );
        break;
      case _WATER:
        
        agent = new Water( mWorld, *new b2Vec2( position.x / mWorldWidth, position.y / mWorldHeight ), mVertices );
        break;
      case _BOUNDARY:
        
        agent = new Rock( mWorld, *new b2Vec2( position.x / mWorldWidth, position.y / mWorldHeight ), mVertices );
        break;
        
      default:
        agent = NULL;
        break;
    }
    
    return agent;
  }
  
  void create( agentFixtureType type, b2Vec2 &position,
                vector<b2Vec2*> mVertices = vector<b2Vec2*>() );
  void update();
  void draw();
  void drawGui();
  XmlTree getXmlString();
  
  void setWorldWidth( float width ){ mWorldWidth = width; }
  void setWorldHeight( float height ){ mWorldHeight = height; }
  
  float mWorldWidth = 1.0f; // default
  float mWorldHeight = 1.0f; // default
private:
  b2World         *mWorld;
  vector<Agent*>  mAgents;

  
};