//
//  Particle.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/02/19.
//
//

#pragma once
#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"
#include "cinder/Rand.h"

#include <Box2D/Box2D.h>

#include "Resources.h"
#include "Predator.h"
#include "Herbivore.h"

using namespace ci;
using namespace ci::app;

class Herbivore;
class Predator;

class Particle {
public:
  Particle( b2World *mSandbox, b2Vec2 &pos, float radius, uint16 categoryBits, uint16 maskBits );
  ~Particle();
  
  //static Particle *Create( agentFixtureType type );
  
  virtual void update();
  virtual void draw();
  
  void setAsHero();

  void pullToCenter();
  
  // Sensors
  void visionAcquiredEnemy(Particle *enemy);
  void visionLostEnemy(Particle *enemy);
  
  void startContact(){ mContact = true; }
  void endContact(){ mContact = false; }
  // UserData can be used to store current ray scan angle in the particle
  // Can be used for neural network data, emotional model, etc
  
  // boids
  void applyLinearImpulse( b2Vec2 impulse );
  
  b2Vec2  mPosition;
  b2Vec2  mLinearVelocity;
  float   mAngle;
  float   mRadius;
  float   mCircleRadius;
  float32 angle;
  float   initialRotation;
  
  bool    mHero;
  bool    mContact;
  bool    mFriend;

  b2Body  *body; // should be private / protected
  std::vector<Particle*>    mVisible;
  
private:
  b2World *mWorld;

//  std::vector<TDRTire*>     m_tires;
//  b2RevoluteJoint *flJoint, *frJoint;
  
  b2BodyDef           bodyDef;
  b2PolygonShape      dynamicBox;
  b2FixtureDef        fixtureDef;
  b2RevoluteJointDef  jointDef;

  uint16    particleType;
  uint16    particleState;
  uint16    particleSubState;
  uint16    particleEmotion;

  // BOIDS
  float mZoneRadius = 30.0f * 30.0f * 10.0f;
  float mThreshold = 65.0f;
  float mHighThreshold = 0.75f;

};

