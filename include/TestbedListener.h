//
//  TestbedListener.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/02/27.
//
//  QueryCallback taken from box2d testbed

#pragma once

#include "cinder/app/AppNative.h"
#include "cinder/Rect.h"
#include "cinder/Vector.h"
#include "cinder/Rand.h"

#include <Box2D/Box2D.h>
#include <memory> // smart pointers

#include "Resources.h"

#include "MayaOrthoCamUI.h"

using namespace ci;
using namespace ci::app;
using namespace std;


class QueryCallback : public b2QueryCallback {
public:
	QueryCallback(const b2Vec2& point){
		m_point = point;
		m_fixture = NULL;
	}
	
	bool ReportFixture(b2Fixture* fixture){
		b2Body* body = fixture->GetBody();
		if (body->GetType() == b2_dynamicBody){
			bool inside = fixture->TestPoint(m_point);
			if (inside){
				m_fixture = fixture;
				
				// We are done, terminate the query.
				return false;
			}
		}
		
		// Continue the query.
		return true;
	}
	
	b2Vec2 m_point;
	b2Fixture* m_fixture;
};
