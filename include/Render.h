#pragma once
#include "Render.h"
#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"
#include "cinder/Rand.h"

#include <Box2D/Box2D.h>

#include "Resources.h"
//#include "Agent.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class Render : public b2Draw {
public:
	Render();
	~Render();
  
	void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
	void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
	void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);
	void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);
	void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);
	void DrawTransform(const b2Transform& xf);
	void DrawPoint(const b2Vec2& p, float32 size, const b2Color& color);
	void DrawString(int x, int y, const char* string, ...);
	void DrawAABB(b2AABB* aabb, const b2Color& color);
  
private:
	int drawShape = 1;
	int drawJoint = 1;
	int drawAABB = 0;
	int drawPairs = 0;
	int drawCenterOfMass = 1;
  
	void updateFlags();
};