//
//  Water.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/05/13.
//
//

#pragma once
#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"
#include "cinder/Rand.h"

#include <Box2D/Box2D.h>
#include "Agent.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class Water : public Agent  {
public:
  Water( b2World* mSandbox, b2Vec2 &position, vector<b2Vec2*> mVertices = vector<b2Vec2*>() );
  ~Water(){}
  
  void update();
  void draw();
  void drawGui(){}
  void pullToCenter();
  void boids();
  void toggleInfo(){}
    bool getHealth(){ return mIsDead; }
  
  string getXmlString();
   XmlTree getXmlVertices();
  
  void applyLinearImpulse( b2Vec2 impulse ){}
  b2Vec2 getLinearVelocity(){ return mLinearVelocity; }
  
  // Sensors
  void visionAcquiredEnemy( Agent *agent ){}
  void visionLostEnemy( Agent *agent ){}
  // Contact listener
  void startContact(){ mContact = true; }
  void endContact(){ mContact = false; }
  
  b2Body *getBody(){
    return body;
  }
  
  agentFixtureType getAgentType(){
    return mAgentType;
  }
  
  void setHero(){}
  void unsetHero(){}
  
  void setHealth( float value );
  
private:
  b2World *mWorld;
  b2Body  *body;
  
  b2BodyDef           bodyDef;
  b2PolygonShape      dynamicBox;
  b2FixtureDef        fixtureDef;
  b2RevoluteJointDef  jointDef;
  
  bool    mHero;
  bool    mContact;
  b2Vec2  mPosition;
  b2Vec2  mLinearVelocity;
  float   mAngle;
  float   mCircleRadius;
  int verticesCount;
  b2Vec2 *vertices;
  
  uint16 categoryBits = WATER;
  uint16 maskBits = BOUNDARY | PREDATOR | HERBIVORE;
  agentFixtureType mAgentType = _WATER;
  
  
  vector<Agent*>    mVisible;
  
  // STATS
  float mHunger;
  bool  mIsHungry;
  bool  mIsDead;
  float health = 100;
  
};