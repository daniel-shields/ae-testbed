//
//  Herbivore.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/04/15.
//
//

#pragma once
#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"
#include "cinder/Rand.h"

#include <Box2D/Box2D.h>
#include "Agent.h"
#include "Model.h"
#include "Monitor.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class Herbivore : public Agent {
public:
  Herbivore( b2World* mSandbox, b2Vec2 &position );
  ~Herbivore();
  void update();
  void draw();
  void drawGui();
  void pullToCenter();
  void boids();
  void model();
  void toggleInfo();
  void behaviour();
  bool getHealth(){ return mIsDead; }
  XmlTree getXmlVertices();
  
  void applyLinearImpulse( b2Vec2 impulse ){
    body->ApplyLinearImpulse(impulse, body->GetWorldCenter());
  }
  b2Vec2 getLinearVelocity(){ return mLinearVelocity; }
  
  string getXmlString();
  
  // Sensors
  void visionAcquiredEnemy( Agent *agent );
  void visionLostEnemy( Agent *agent );
  // Contact listener
  void startContact(){ mContact = true; std::cout << "contact started\n"; }
  void endContact(){ mContact = false; }

  b2Body *getBody(){
    return body;
  }
  
  agentFixtureType getAgentType(){
    return mAgentType;
  }
  
  void setHero(){ mHero = true; }
  void unsetHero(){ mHero = false; mInfo = false; }
  void setHealth( float value ){ mHealth += value;  }

  friend class Agent;
  
private:
  b2World *mWorld;
  b2Body  *body;
  vector<Agent*>    mVisible;
  Model   *mModel;
  
  b2BodyDef           bodyDef;
  b2PolygonShape      dynamicBox;
  b2FixtureDef        fixtureDef;
  b2RevoluteJointDef  jointDef;
  b2CircleShape       circleShape1;
  
  bool    mHero;
  bool    mContact;
  bool    mInfo = false;
  b2Vec2  mPosition;
  b2Vec2  mLinearVelocity;
  float   mAngle;
  float   mCircleRadius;
  int verticesCount;
  
  uint16 categoryBits = HERBIVORE;
  uint16 maskBits = BOUNDARY | PREDATOR | HERBIVORE | FOOD;
  agentFixtureType mAgentType = _HERBIVORE;
  
  // BOIDS
  float mZoneRadius = 30.0f * 30.0f * 10.0f;
  float mThreshold = 65.0f;
  float mHighThreshold = 0.75f;
  float mAttractStrength = 0.004;
  float mRepelStrength = 0.01f;
  float mOrientStrength = 0.01f;
  
  // Physics
  float mMaxForwardSpeed;
  float mMaxBackwardSpeed;
  float mMaxDriveForce;
  float mMaxLateralImpulse;
  float mLastDriveImpulse;
  float mLastLateralFrictionImpulse;
  float mCurrentTraction;
  b2Vec2 pointOfInterest;
  
  // STATS
  //float mFear;
  float mCrowdFactor;
  bool  mIsDead;
  bool  mFollowed;
  //float mHunger;
  bool  mIsHungry;
  //float mThirst;
  bool  mIsThirsty;
  
  float mHealth;

};