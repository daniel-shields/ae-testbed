//
//  MayaOrthoCamUI.h
//  AETestbed
//
//  Created by Daniel Shields on 2014/02/26.
//
//  Adaption of Cinder / include / cinder / MayaCamUI.h
//  for an Orthographic style Camera
//

#pragma once

#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/Vector.h"
#include "cinder/Text.h"
#include "cinder/Camera.h"
#include "cinder/params/Params.h"

#include "Resources.h"

using namespace ci;
using namespace ci::app;

class MayaOrthoCamUI {
public:
  MayaOrthoCamUI(){}
  MayaOrthoCamUI( const CameraPersp &aInitialCam ){
    mInitialCam = mCurrentCam = aInitialCam;
  }
  
  ~MayaOrthoCamUI(){}
  
    const CameraPersp &getCamera() const {
        return mCurrentCam;
    }
    void setCurrentCam( const CameraPersp &aCurrentCam ){
        mCurrentCam = aCurrentCam;
    }
    
    void centerCamera();
    void focusZoom( float focusDistance = 250.0f );
    void focusCamera( const Vec2f focusPoint );
    void zoomCamera( const Vec2i &mousePos );
    void panCamera( const Vec2i &mousePos );
    void aspectRation( float ratio );
    void update();
    void showParams();
    void zoomParamCamera( float distance );
  
    void mouseDown( const Vec2i &mousePos );
    void mouseUp( const Vec2i &mousePos );
    const Vec2f mouseAdjust( const Vec2i &mousePos );

    Vec2f screenToWorldConversion(const Vec2f & point);
  
private:

    Vec2i           mInitialMousePos;
    CameraPersp     mCurrentCam, mInitialCam;
    int             mLastAction;
    float mCameraDistance = 10.0f;
    Vec3f mEye = Vec3f(0.0f, 0.0f, mCameraDistance);
  Vec3f mCenter = Vec3f::zero();
  Vec3f mUp = Vec3f::yAxis();

    // type one
    Vec3f screenToWorld(Vec2f mousePos, float depth=0.0f);
    Vec2i mMousePos;
    float mMouseDepth = 0.0f;
  
    // type two
    Matrix44f mModelView;
    Matrix44f mProjection;

    Vec3f unprojectConversion(const Vec3f & point);
    Area mViewport;
    Rectf mWindowSize;

  
};