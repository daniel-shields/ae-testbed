//
//  BoundaryElement.cpp
//  AETestbed
//
//  Created by Daniel Shields on 2014/02/18.
//
//

#include "BoundaryElement.h"

BoundaryElement::BoundaryElement( b2World &mSandbox, AgentFactory &mFactory ){
    mWorld = &mSandbox;
    mAgentFactory = &mFactory;
  
//    mAgentFactory = new AgentFactory( mSandbox );
}


BoundaryElement::~BoundaryElement(){
  
}
/**
 * Create a chain shape with isolated vertices, chains allow multiple
 * edges (line segement) to be connected together, creating a free-form
 * static environment for the world.
 *
 */
void BoundaryElement::createBoundaryElement(){
    
    // Center the shape in the window
    groundBodyDef.position.Set( 0, 0 );
    groundBodyDef.type = b2_staticBody;
  
    edgeVertices[0].Set( 0, 0 );
    edgeVertices[1].Set( mWorldWidth, 0 );
    edgeVertices[2].Set( mWorldWidth, mWorldHeight );
    edgeVertices[3].Set( 0, mWorldHeight );
    
    // Create chain loop, the first and last vertices are automatically linked
    chain.CreateLoop(edgeVertices, 4);
  
    // Add the chain fixture to the world
   b2Body* groundBody = mWorld->CreateBody( &groundBodyDef );
   groundBody->CreateFixture( &chain, 0.0f );

}




void BoundaryElement::loadMap(string pathToFile){
  
  string file = "/Users/irae/Development/xcode/ae-testbed/" + pathToFile;

  try {
    XmlTree doc( loadFile(  file ) );
    XmlTree world = doc.getChild( "map/world" );
    mWorldLoaded = true;
    
    // SETTINGS
    XmlTree settings = doc.getChild( "map/settings" );
    mWorldHeight = settings.getAttributeValue<int>("height");
    mWorldWidth  = settings.getAttributeValue<int>("width");
    mWorldName   = settings.getChild( "name" ).getValue();
    mWorldOwner  = settings.getChild( "owner" ).getValue();

    // boundary
    createBoundaryElement();
    
    // WORLD
    for( XmlTree::Iter element = world.begin(); element != world.end(); ++element ){
    
      string elementType = element->getAttributeValue<string>("type");
      float32 posx = element->getAttributeValue<float32>("posx");
      float32 posy = element->getAttributeValue<float32>("posy");
    
   //   cout << elementType << "\n";
      
      // ELEMENT ATTRIBUTES
      if( element->hasChild( "attributes" ) ){
        element->getChild("attributes");
   //     cout << "  attributes" << "\n";
//        cout << "  -density: " <<
//                element->getChild("attributes/density").getValue() << "\n";
//        cout << "  -resistance:" <<
//                element->getChild("attributes/resistance").getValue() << "\n";
      }
      


      // ELEMENT GEOMETRY
      // move coords from vector to vertices array
      int verticesCount;
      b2Vec2 *vertices;
      
      if( element->hasChild( "geometry" ) ){
        XmlTree geometry = element->getChild("geometry");
        for( XmlTree::Iter coord = geometry.begin(); coord != geometry.end(); ++coord ){
//          cout << "  " << coord->getTag() << "\n";
          
          // populate vector list with coords
          int pointx = coord->getAttributeValue<int>("x");
          int pointy = coord->getAttributeValue<int>("y");
          mVertices.push_back( new b2Vec2(pointx, pointy) );
//          cout << "edge: " << pointx << ", " << pointy << "\n";
          
        }
        
        // move coords from vector to vertices array
        verticesCount = mVertices.size();
        vertices = new b2Vec2[ verticesCount ];
        for (int i = 0; i < verticesCount; i++){
          vertices[ i ].Set( mVertices[i]->x, mVertices[i]->y );
        }
      }


      // create physics body
      if(elementType == "herbivore"){
        
        b2Vec2 position = *new b2Vec2( posx, posy );
        mAgentFactory->create( _HERBIVORE, position );
        
      } else if(elementType == "predator"){
        
        b2Vec2 position = *new b2Vec2( posx, posy );
        mAgentFactory->create( _PREDATOR, position );
        
      } else if(elementType == "food"){
        
        b2Vec2 position = *new b2Vec2( posx, posy );
        mAgentFactory->create( _FOOD, position );
        
      } else if(elementType == "rock"){
        b2Vec2 position = *new b2Vec2( posx, posy );
        mAgentFactory->create( _BOUNDARY, position, mVertices );
     
      } else if(elementType == "water"){
        b2Vec2 position = *new b2Vec2( posx, posy );
        mAgentFactory->create( _WATER, position, mVertices );
  
      }

      // Clear coordinates vector
      mVertices.clear();
    }
    
    
    
    // draw
    mAgentFactory->setWorldHeight( mWorldHeight );
    mAgentFactory->setWorldWidth( mWorldWidth );

    
  } catch( XmlTree::ExcChildNotFound error ){
    cout <<  "XmlTree: Child not found error.\n";
  }
  
}




void BoundaryElement::saveMap(){
  
  time_t now = time(0);
  string timestamp = boost::lexical_cast<string>(now);
  
  string pathToFile = "~/Development/xcode/AETestbed/map/" + timestamp + "_map.xml";
  
  XmlTree map("map", "");
  XmlTree settings("settings", "");
  settings.setAttribute("height", mWorldHeight);
  settings.setAttribute("width", mWorldWidth);
  settings.push_back( XmlTree("name", mWorldName) );
  settings.push_back( XmlTree("owner", mWorldOwner) );
  map.push_back(settings);
    
  XmlTree world = mAgentFactory->getXmlString();
  map.push_back(world);
  
  map.write( writeFile(pathToFile) );
  cout << "file saved.\n";
  
}

b2Vec2 BoundaryElement::getWorldSize(){
  return b2Vec2( mWorldHeight, mWorldWidth );
}


void BoundaryElement::draw(){

  // Draw a line for each child edge
  for(int32 i = 0; i < chain.GetChildCount(); ++i){
    b2EdgeShape edge;
    chain.GetChildEdge(&edge, i);

    gl::color( Color(1, 1, 1) );

    Vec2f pointx = *new Vec2f( (edge.m_vertex0.x), (edge.m_vertex0.y ));
    Vec2f pointy = *new Vec2f( (edge.m_vertex1.x), (edge.m_vertex1.y ));
    
    glPushMatrix();
    gl::drawLine( pointy, pointx );
    glPopMatrix();
  }
  
  // Draw background
  if( mWorldLoaded ){
    gl::color( COLOUR_BG );
    Rectf rect = Rectf(0, 0, mWorldWidth, mWorldHeight);
    gl::drawSolidRect(rect);
  }
  
}
