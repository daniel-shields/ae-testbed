//
//  Herbivore.cpp
//  AETestbed
//
//  Created by Daniel Shields on 2014/04/15.
//
//

#include "Herbivore.h"

Herbivore::Herbivore( b2World* mSandbox, b2Vec2 &position ){
  
  mWorld = mSandbox;
  
  // CREATE BODY
  body = NULL;
  bodyDef.type = b2_dynamicBody;
  bodyDef.position.Set(position.x, position.y);
  body = mWorld->CreateBody( &bodyDef );
  
  
  // DEFINE BODY SHAPE
  verticesCount = 6;
  b2Vec2 vertices[6];
  vertices[0].Set(    0,    0);
  vertices[1].Set(    3,  2.5);
  vertices[2].Set(  2.8,  5.5);
  vertices[3].Set(    0,   10);
  vertices[4].Set( -2.8,  5.5);
  vertices[5].Set(   -3,  2.5);
  dynamicBox.Set( vertices, 6);
    
  // DEFINE BODY FIXTURE
  fixtureDef.shape = &dynamicBox;
  fixtureDef.density = 1.0f;
  fixtureDef.friction = 0.6f;
  fixtureDef.isSensor = false;
  fixtureDef.restitution = 0.1f; // bounce
  fixtureDef.filter.categoryBits = categoryBits;
  fixtureDef.filter.maskBits = maskBits;
  
  // ADD FIXTURE TO WORLD
  body->CreateFixture( &fixtureDef );

  
  // SENSOR
  circleShape1.m_p.Set( 0.0f, 0.0f );
  mCircleRadius = 80.0f;
  circleShape1.m_radius = mCircleRadius;
  fixtureDef.shape = &circleShape1;
  fixtureDef.density = 0;
  fixtureDef.isSensor = true;
  fixtureDef.filter.categoryBits = SENSOR;
  fixtureDef.filter.maskBits = HERBIVORE | PREDATOR | FOOD ;
  // ADD FIXTURE TO WORLD
  body->CreateFixture( &fixtureDef );
  
  // STATS
  //mFear = 1.0f;
  mCrowdFactor = 1.0f;
  mIsDead = false;
  mFollowed = false;
  //mHunger = 1.0f;
  mIsHungry = true;
  //mThirst = 1.0f;
  mIsThirsty = true;

  // DEFINE USER DATA
  mHero = false;
  mContact = false;
  body->SetUserData( this );
  
  mModel = new Model();
  
  mPosition = body->GetPosition();
  mAngle = body->GetAngle();
  body->SetLinearVelocity(b2Vec2(1,1));

  // car
  body->SetAngularDamping(5);
  
  mMaxDriveForce = 100; //?
  mMaxForwardSpeed = 10;
  mMaxBackwardSpeed = -5;
  mMaxLateralImpulse = 4; //?
  
  mLastDriveImpulse = 1;
  mLastLateralFrictionImpulse = 1;
  mCurrentTraction = 1;
  pointOfInterest =  { 2000.0f * 2500, 2000.0f * 3500 };
  
  mHealth = 100.0f;
}

Herbivore::~Herbivore(){
  
  delete mModel;
  
  b2Fixture* fixtures = body->GetFixtureList();
  while( fixtures != NULL ){
      body->DestroyFixture(fixtures);
  }
  
  mWorld->DestroyBody(body);
  
}

void Herbivore::pullToCenter(){

   Vec2f center { 4000, 4000 };
   
   b2Vec2 pos = body->GetPosition();
   Vec2f dirToCenter = Vec2f{pos.x, pos.y} - center;
   b2Vec2 tempDirForceToCenter = b2Vec2 { dirToCenter.x, dirToCenter.y };
   
   // experiement force to pull to centre is too strong, so lowering it
   // may be the box2d world loop is x10, may turn that off instead?
   tempDirForceToCenter = b2Vec2 {tempDirForceToCenter.x / 500.0f, tempDirForceToCenter.y / 500.0f};
   // endexperiment
   
   float disToCenter = dirToCenter.length();
   float maxDistance = 300.0f;
   
   if( disToCenter > maxDistance ){
     dirToCenter.normalize();
     // float pullStrength = 0.001f;
     // apply force to pull particle to center
     // b2Vec2 tempDir = b2Vec2{ dirToCenter.x, dirToCenter.y };
     b2Vec2 force = - tempDirForceToCenter;
     body->ApplyForce( force, body->GetWorldCenter() );
     
   }
}

void Herbivore::boids(){
  // BOIDS
  for(int i=0; i < mVisible.size(); i++){
    Agent* agent = (Agent*)mVisible[i]->getBody()->GetUserData();
    
    b2Vec2 travel = body->GetPosition() - agent->getBody()->GetPosition();
    float distanceSquared = travel.LengthSquared();
    float percent = ( mZoneRadius/distanceSquared );
    
    if( percent < mThreshold ){
      // SEPERATION
      float F = ( mThreshold / percent - 1.0f ) * mRepelStrength;
      F *= 0.1f; // test
      travel.Normalize();
      travel *= F;
      
      pointOfInterest = { travel.x, travel.y };
      
//      this->applyLinearImpulse( travel );
//      agent->applyLinearImpulse( -travel );
      
    } else if( percent < mHighThreshold ) {
      // ALIGNMENT
      float threshDelta = mHighThreshold - mThreshold;
      float adjust = ( percent - mThreshold ) / threshDelta;
      float F = ( 1.0 - ( cos(adjust * (M_PI * 2.0f)) * 0.5f + 0.5 ) *  mOrientStrength );
      
      //b2Vec2 changeFirst = b2Vec2{ agent->getLinearVelocity().x * F,  agent->getLinearVelocity().y * F };
      b2Vec2 changeSecond = b2Vec2{ getLinearVelocity().x * F,
        getLinearVelocity().y * F };
      
       pointOfInterest = changeSecond;
      
//      this->applyLinearImpulse(changeFirst);
//      agent->applyLinearImpulse(changeSecond);
      
    } else {
      // COHESION
      float threshDelta = 1.0f - mThreshold;
      float adjust = (percent - mThreshold) / threshDelta;
      
      float F = ( 1.0 - ( cos(adjust * (M_PI * 2.0f)) * 0.5f + 0.5f ) * mRepelStrength );
      
      travel.Normalize();
      travel *= F;
      
      pointOfInterest = { travel.x, travel.y };
//      this->applyLinearImpulse( -travel );
//      agent->applyLinearImpulse( travel );
      
    }
    
  }

}

void Herbivore::model(){
  
  for(int i=0; i < mVisible.size(); i++){
    Agent* agent = (Agent*)mVisible[i]->getBody()->GetUserData();
  
    // eating if touching food
    if ( agent->getAgentType() == _FOOD ){
        // temp
      if( mIsHungry ){ pointOfInterest = agent->getBody()->GetPosition(); }
      
      if( abs(agent->getBody()->GetPosition().x - body->GetPosition().x) < 25.0f
          && abs(agent->getBody()->GetPosition().y - body->GetPosition().y) < 25.0f){
         if( mIsHungry ){ // only eat when hungry
            //cout << "touching food\n";
            mModel->setSensation(mEating, 0.01f);
            mModel->setSensation(mHunger, -0.1f);
            agent->setHealth( -1.0f );
          } else {
            mModel->setSensation(mEating, -0.1f);
          }
      }
   
    }
    
    // eating if touching food
    if ( agent->getAgentType() == _WATER ){
      if( mIsThirsty ){ pointOfInterest = agent->getBody()->GetPosition(); }
      
      if( abs(agent->getBody()->GetPosition().x - body->GetPosition().x) < 15.0f
         && abs(agent->getBody()->GetPosition().y - body->GetPosition().y) < 15.0f){
        
        //cout << "touching water\n";
        mModel->setSensation(mDrinking, 0.001f);
        mModel->setSensation(mThirst, -0.01f);
        
      } else {
        mModel->setSensation(mDrinking, -0.0001f);
      }
    }
    
    
    // add pain if touching boundary
    if ( agent->getAgentType() == _BOUNDARY ){
      
      if( abs(agent->getBody()->GetPosition().x - body->GetPosition().x) < 15.0f
         && abs(agent->getBody()->GetPosition().y - body->GetPosition().y) < 15.0f){
        
        //cout << "touching boundary\n";
        mModel->setSensation(mPain, 0.01f);
        
      } else {
        mModel->setSensation(mPain, -0.001f);
      }
    }
    
    // mCrowding = friends in area
    // mThreat = enemies in aread
    
    // if touching predator, deal it a small amount of damage, so that ~4 herbivores could kill a predator
  }
}


void Herbivore::visionAcquiredEnemy( Agent *agent ){
  //std::cout << "vision acquired\n";
  mVisible.push_back( agent );
  mContact = true;
  
  // Stats
  mCrowdFactor -= ( mCrowdFactor - ( 1.0f - mVisible.size() * 0.02f ) ) * 0.1f;
  mCrowdFactor = constrain( mCrowdFactor, 0.5f, 1.0f );
  
  if(agent->getAgentType() == _HERBIVORE ){
    mModel->setSensation(mCrowding, 0.1);
    mModel->setSensation(mThreat, -0.1);
  } else if ( agent->getAgentType() == _PREDATOR ){
    mModel->setSensation(mThreat, 0.3); // increase fear sensation
  } else if ( agent->getAgentType() == _FOOD ){
    mModel->setSensation(mSmell, 0.1f);
  } else if ( agent->getAgentType() == _WATER ){
    mModel->setSensation(mHumidity, 0.1f);
  }

}

void Herbivore::visionLostEnemy( Agent* agent ){
  
  if(agent->getAgentType() == _HERBIVORE ){
    mModel->setSensation(mCrowding, -0.1f);
    mModel->setSensation(mThreat, 0.1f);
  } else if ( agent->getAgentType() == _PREDATOR ){
    mModel->setSensation(mThreat, -0.3f);
  } else if ( agent->getAgentType() == _FOOD ){
    mModel->setSensation(mSmell, -0.1f);
  } else if ( agent->getAgentType() == _WATER ){
    mModel->setSensation(mHumidity, -0.1f);
  }

  
  //std::cout << "vision lost \n";
  mVisible.erase( std::find(mVisible.begin(), mVisible.end(), agent ));
  
  if( mVisible.size() == 0){
    mContact = false;
  }

}

void Herbivore::update(){
  
  // Basic settings
  mPosition = body->GetPosition();
  mAngle = body->GetAngle();
  mLinearVelocity = body->GetLinearVelocity();

  // Pull agents to center of map, "gravity"
  //pullToCenter();
  
  boids();
  model();

  // layer 0, sensations
  mModel->setSensation(mHunger, 0.0001f);
  mModel->setSensation(mThirst, 0.0001f);
  
  if( mModel->getFeeling(mHunger) > 0.1f){
    mIsHungry = true;
    mHealth -= 0.001;
  } else {
    mIsHungry = false;
  }
  
  // calculate_sensations();
  //
  mModel->update();
  
  if( mHero ){
    // selected as hero, show params monitor
    mModel->toggle(true);
  } else {
    mModel->toggle(false);
    // no longer hero, remove params monitor
  }
  
  // behaviours
  behaviour();
  
  if( mHealth == 0){
    mIsDead = true;
  }
  
  if( mIsDead ){
    body->SetAwake(false);
  }

}

void Herbivore::behaviour(){
  _entitySubState movement = mModel->get_behaviour();

  float desiredSpeed = mMaxForwardSpeed;
  float travelX;
  float travelY;

  float degrad = 0.0174532925199432957f;
  float lockAngle = 35* degrad;

  // for test let them angle to the center, later they should angle towards or away from a point of interest

  pointOfInterest = b2Vec2( 2000, 2000);
  
  b2Vec2 toTarget = pointOfInterest - body->GetPosition();
  float desiredAngle = atan2f( -toTarget.x, toTarget.y );
  
  
  // apply impulse method
  float totalRotation = desiredAngle - mAngle;
  // float angle = mAngle * 180.0f/M_PI;

  float change = 1 * degrad; // deg to rad

  // safety to stop strange updates when center of interest is below body
  while ( totalRotation < -180 * degrad ) totalRotation += 360 * degrad;
  while ( totalRotation >  180 * degrad ) totalRotation -= 360 * degrad;
  
  float newAngle = mAngle + b2Min( change, b2Max(-change, totalRotation));
  
  // set impulse in the direction of the angle
  float F = ( 1.0 - ( cos(newAngle * (M_PI * 2.0f)) * 0.5f + 0.5 ) *  totalRotation );
  
  body->SetTransform( body->GetPosition(), newAngle );
  // angle
  //body->SetAngularVelocity(0); // may be necessary:  eliminate the effects of the angular velocity from the previous time step
  
  switch( movement ){
    case MOVE_FWRD:
      // travel in direction of angle
//       cout << "F: " << F << "\n";
      travelX = this->getLinearVelocity().x * F;
      travelY = this->getLinearVelocity().y * F;
 //     cout << "travel: " << travelX << ". " << travelY << "\n";
      
      break;
    case MOVE_BKWD:
      travelX = this->getLinearVelocity().x * F;
      travelY = this->getLinearVelocity().y * F;

      travelX = -travelX;
      travelY = -travelY;

      desiredSpeed = mMaxBackwardSpeed;
      // travel in direction of angle
//      F = ( 1.0 - ( cos(newAngle * (M_PI * 2.0f)) * 0.5f + 0.5 ) *  -rotationUpdate );
//      travelX = this->getLinearVelocity().x * F;
//      travelY = this->getLinearVelocity().y * F;
      break;
    case MOVE_XPLR:
      
      // change angle, move forward
      pointOfInterest = { randFloat(-200, 200), randFloat(-200, 200) };
      travelX = this->getLinearVelocity().x * F;
      travelY = this->getLinearVelocity().y * F;
      break;
    case MOVE_STOP:
      desiredSpeed = 0;
      // need to confirm
      // stop spinning
      travelY = 0.0f;
      travelX = 0.0f;
      break;
  }
  
  // CAR DRIVE PHYSICS
  // update max speed
      // get forward velocity
      b2Vec2 currentForwardNormal = body->GetWorldVector( b2Vec2(0,1) );
      b2Vec2 currentVel =  b2Dot( currentForwardNormal, body->GetLinearVelocity() ) * currentForwardNormal;
      // get lateral velocity
      b2Vec2 currentRightNormal = body->GetWorldVector( b2Vec2(1,0) );
      b2Vec2 lateralVel =  b2Dot( currentRightNormal, body->GetLinearVelocity() ) * currentRightNormal;
  //find current speed in forward direction
      b2Vec2 currentForwardNormal2 = body->GetWorldVector( b2Vec2(0,1) );
      float currentSpeed = b2Dot( currentVel, currentForwardNormal2 );
  // apply necessary force
      float force = 0;
      if( desiredSpeed > currentSpeed ) { force = mMaxDriveForce; }
      else{ force -= mMaxDriveForce * 0.5f; }
  // calculate force
      float speedFactor = currentSpeed / 120;
      b2Vec2 driveImpulse = (force / 60.0f) * currentForwardNormal;
      if ( driveImpulse.Length() > mMaxLateralImpulse )
        driveImpulse *= mMaxLateralImpulse / driveImpulse.Length();
  
      b2Vec2 lateralFrictionImpulse = body->GetMass() * -lateralVel;
  
      float lateralImpulseAvailable = mMaxLateralImpulse;
      lateralImpulseAvailable *= 2.0f * speedFactor;
  
      if ( lateralImpulseAvailable < 0.5f * mMaxLateralImpulse )
        lateralImpulseAvailable = 0.5f * mMaxLateralImpulse;
      if ( lateralFrictionImpulse.Length() > lateralImpulseAvailable )
        lateralFrictionImpulse *= lateralImpulseAvailable / lateralFrictionImpulse.Length();
  // apply force
      mLastDriveImpulse = driveImpulse.Length();
      mLastLateralFrictionImpulse = lateralFrictionImpulse.Length();
      
      b2Vec2 impulse = driveImpulse + lateralFrictionImpulse;
      if ( impulse.Length() > mMaxLateralImpulse )
        impulse *= mMaxLateralImpulse / impulse.Length();
      body->ApplyLinearImpulse( mCurrentTraction * impulse, body->GetWorldCenter() );
  // end update max speed

  
}

void Herbivore::drawGui(){
    if( mHero ){
      mModel->draw();
    }
}

void Herbivore::toggleInfo(){
  if( mHero ){
  mInfo = !mInfo;
  mModel->info();
  }
}

void Herbivore::draw(){

  
  if( body == NULL ) {    // safety
    return;
  }

  // convert angle radians to deg
  float angle = mAngle * 180.0f/M_PI;
  
  _entityEmotion emo = mModel->getDominantEmotion();
  
  
  if( mHero ){
  //  gl::color( ColorA( 0.97f, 0.10f, 0.58f, 0.8f ) );
  } else if( mContact ){
    // gl::color( ColorA( 0.10f, 0.97f, 0.58f, 0.8f ) );
  } else if( mIsDead ) {
    gl::color( COLOUR_DEAD );
  }
  
  // DRAW FIXTURES
  
  b2Fixture* fixtures = body->GetFixtureList();
  while( fixtures != NULL ){
    
    switch( fixtures->GetType() ){
        
      case b2Shape::e_polygon: {
        switch( emo ){
          case 1: gl::color( COLOUR_HAPPY ); break;
          case 2: gl::color( COLOUR_SAD ); break;
          case 3: gl::color( COLOUR_FEAR ); break;
          case 4: gl::color( COLOUR_ANGRY ); break;
          case 5: gl::color( COLOUR_NONE ); break;
        }
          b2PolygonShape* shape = (b2PolygonShape*) fixtures->GetShape();
          
          glPushMatrix();
          
          gl::translate( Vec2f{ mPosition.x, mPosition.y } );
          gl::rotate( angle );
          glBegin(GL_POLYGON);
          
          for( int i = 0; i != shape->GetVertexCount(); ++i ){
            gl::vertex( shape->GetVertex(i).x, shape->GetVertex(i).y );
          }
          
          glEnd();
          glPopMatrix();
        
      }
      case b2Shape::e_circle: {
        
        if (mHero){
            //gl::color( ColorA( 0.47f, 0.66f, 0.10f, 0.3f ) ); }
            gl::color( COLOUR_HERO );
          gl::drawStrokedCircle( Vec2f(mPosition.x, mPosition.y), mCircleRadius );
        }

      }
      default:
        break;
    }
    fixtures = fixtures->GetNext();
    
  }

  
  
  // VISION SENSOR
  if (mContact){ // draw a line from this agent to the agent in vision
    for(int i=0; i < mVisible.size(); i++){

      Agent* agent = (Agent*)mVisible[i]->getBody()->GetUserData();
      b2Vec2 enemyPos = agent->getBody()->GetPosition();
      
      Vec2f start { enemyPos.x, enemyPos.y };
      Vec2f end   { mPosition.x, mPosition.y };
      
      gl::color( COLOUR_WHITE );
      gl::drawLine(start, end);
      
    }
  }

}

string Herbivore::getXmlString(){
  return "herbivore";
}

XmlTree Herbivore::getXmlVertices(){
  XmlTree geometry("geometry", "");
  
  for( int i =0; i < verticesCount; i++){
  
    b2Vec2 temp = dynamicBox.m_vertices[i];

    XmlTree coords("coords", "");
    coords.setAttribute("x", (int)temp.x );
    coords.setAttribute("y", (int)temp.y );
    geometry.push_back(coords);
    
  }
  
  return geometry;
}

