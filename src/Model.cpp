//
//  Model.cpp
//  AETestbed
//
//  Created by Daniel Shields on 2014/05/05.
//
//

#include "Model.h"

Model::Model(){
  mMonitor = new Monitor();
  mMonitor->addDependancyMonitor( mCoeficient );

  mNeuralNetwork = new NeuralNetwork();
  
  // actions
  mNeuralActionFWRD = new NeuralNetwork();
  mNeuralActionBWRD = new NeuralNetwork();
  mNeuralActionXPLR = new NeuralNetwork();
  mNeuralActionDONT = new NeuralNetwork();
  
  // info panel
  mOpacity = 1.0f;
  mFadeTime = 500;
}

void Model::update(){
  // layer 1, feelings
  calculate_feelings();
  
  // layer 2, feelings * emotions
  calculate_emotions();
  calculate_hormones();
  
  // layer 3, find dominant emotions
  calculate_dominant();
  calculate_influence(); // calculate late for Hfn to use Afn-1
  // n-1 being the previous rounds emotional influence
  
  // adaptive controller
  // fire for each behaviour
  mNeuralActionFWRD->input( mFeelings );   mNeuralActionFWRD->fire();
  mNeuralActionBWRD->input( mFeelings );   mNeuralActionBWRD->fire();
  mNeuralActionXPLR->input( mFeelings );   mNeuralActionXPLR->fire();
  mNeuralActionDONT->input( mFeelings );   mNeuralActionDONT->fire();

  
  // behaviour selection module
  select_behaviour();
  
  // back-propogate the neural network with the dominant emotion
  learn();
  
  // reinforcements
  //calculate_emotion_based_reinforcement();
  calculate_sensation_based_reinforcement();
  
  // Update display panel
  updateInfo();
}

void Model::updateInfo(){
  
  TextLayout layout;
  TextLayout sensationLayout;
  TextLayout feelingLayout;
  TextLayout hormoneLayout;
  TextLayout infoLayout;
  
  string fps = "Frames Per Second: " + boost::lexical_cast<string>(getFrameRate());
  string tempLine;
  tempLine = "Dominant Emotion: ";
  
  switch( dominantEmotion ){
    case 1: tempLine += "HAPPINESS"; break;
    case 2: tempLine += "SADDNESS"; break;
    case 3: tempLine += "FEAR"; break;
    case 4: tempLine += "ANGER"; break;
  }
  tempLine += "\n";
  
  string behaviourInfo = "Dominant Behaviour: ";
  switch( behaviouralAction ){
    case 0: behaviourInfo += "FORWARD"; behaviour = MOVE_FWRD; break;
    case 1: behaviourInfo += "BACKWARD"; behaviour = MOVE_BKWD; break;
    case 2: behaviourInfo += "EXPLORE"; behaviour = MOVE_XPLR; break;
    case 3: behaviourInfo += "STOP MOVE"; behaviour = MOVE_STOP; break;
    defaul: behaviour = MOVE_FWRD; break;
  }
  
	layout.setFont( Font( "Arial-BoldMT", 14 ) );
	layout.setColor( Color( 1.0f, 1.0f, 1.0f ) );

	layout.addRightLine( "Model info panel" );
	layout.addRightLine( " " );
	layout.setFont( Font( "Arial-BoldMT", 10 ) );
	layout.setColor( Color( 0.6f, 0.6f, 0.6f ) );
	layout.addRightLine( tempLine );
  // behaviour

  behaviourInfo += "\n";
  layout.addRightLine( behaviourInfo );
  layout.addRightLine( fps);
  
  // sensations
  sensationLayout.setFont( Font( "Arial-BoldMT", 14 ) );
	sensationLayout.setColor( Color( 1.0f, 1.0f, 1.0f ) );
  sensationLayout.addLine("Sensations");
  sensationLayout.setFont( Font( "ArialMT", 10 ) );
	sensationLayout.setColor( Color( 0.6f, 0.6f, 0.6f ) );
  
  for( int sensation = 0; sensation < 10; sensation++){
    float t = mSensations[sensation];
    string tempSensation =  boost::lexical_cast<string>( t );
    sensationLayout.addLine( tempSensation );
  }
  
  
  // feelings
  feelingLayout.setFont( Font( "Arial-BoldMT", 14 ) );
	feelingLayout.setColor( Color( 1.0f, 1.0f, 1.0f ) );
  feelingLayout.addLine("Feelings");
  feelingLayout.setFont( Font( "ArialMT", 10 ) );
	feelingLayout.setColor( Color( 0.6f, 0.6f, 0.6f ) );
  //  sensationLayout.addRightLine( get_sensations() );
  for( int feelings = 0; feelings < 10; feelings++){
    float t = mFeelings[feelings];
    string tempFeeling =  boost::lexical_cast<string>( t );
    feelingLayout.addLine( tempFeeling );
  }

  
  // hormones
  hormoneLayout.setFont( Font( "Arial-BoldMT", 14 ) );
	hormoneLayout.setColor( Color( 1.0f, 1.0f, 1.0f ) );
  hormoneLayout.addLine("Hormones");
  hormoneLayout.setFont( Font( "ArialMT", 10 ) );
	hormoneLayout.setColor( Color( 0.6f, 0.6f, 0.6f ) );
  //  sensationLayout.addRightLine( get_sensations() );
  for( int hormones = 0; hormones < 10; hormones++){
    float t = mHormones[hormones];
    string tempHormones =  boost::lexical_cast<string>( t );
    hormoneLayout.addLine( tempHormones );
  }

 // info
  infoLayout.setFont( Font( "Arial-BoldMT", 14 ) );
  infoLayout.setColor( Color( 1.0f, 1.0f, 1.0f ) );
	infoLayout.addRightLine( "\n" );

	infoLayout.setFont( Font( "Arial-BoldMT", 10 ) );
	infoLayout.setColor( Color( 0.6f, 0.6f, 0.6f ) );

	infoLayout.addRightLine( "hunger" );
  infoLayout.addRightLine( "eating" );
  infoLayout.addRightLine( "smell" );
  infoLayout.addRightLine( "thirst" );
  infoLayout.addRightLine( "drinking" );
  infoLayout.addRightLine( "humidity" );
  infoLayout.addRightLine( "pain" );
  infoLayout.addRightLine( "restlessness" );
  infoLayout.addRightLine( "crowding" );
  infoLayout.addRightLine( "threat" );
  
	
  mTexture = gl::Texture( layout.render( true ) );
  mSensationsTexture = gl::Texture( sensationLayout.render( true ) );
  mFeelingsTexture = gl::Texture( feelingLayout.render( true ) );
  mHormonesTexture = gl::Texture( hormoneLayout.render( true ) );
  mInfoTexture = gl::Texture( infoLayout.render( true ) );
}

void Model::toggle( bool state ){
  drawState = state;
}

void Model::draw(){
  if( drawState ){
    mMonitor->draw();
  }
  
  gl::enableDepthRead( true );
  gl::enableDepthWrite( true );
  gl::enableAlphaBlending();
  
  float x = getWindowWidth() - mTexture.getWidth() - 40.0f;
  float y = getWindowHeight() - mTexture.getHeight() - 25.0f;
  float posY = getWindowHeight() - mSensationsTexture.getHeight() - 25.0f;
  
  if( drawInfo ){

    glEnable( GL_TEXTURE_2D );
 
    // texture
    glColor4f( 1, 1, 1, mOpacity );
    gl::draw(mTexture, Vec2f( x, y - mSensationsTexture.getHeight()));
        glDisable( GL_TEXTURE_2D );
    
    
    glEnable( GL_TEXTURE_2D );
    // info
    glColor4f( 1, 1, 1, mOpacity );
    gl::draw(mInfoTexture, Vec2f( x - 380.0f, posY));
    
    glDisable( GL_TEXTURE_2D );
    
       glEnable( GL_TEXTURE_2D );
    // sensations
    glColor4f( 1, 1, 1, mOpacity );
    gl::draw(mHormonesTexture, Vec2f( x - 100.0f, posY ));
    
    glDisable( GL_TEXTURE_2D );
    
    glEnable( GL_TEXTURE_2D );
    // feelings
    glColor4f( 1, 1, 1, mOpacity );
    gl::draw(mFeelingsTexture, Vec2f( x - 200.0f, posY ));
    
    glDisable( GL_TEXTURE_2D );
    
    glEnable( GL_TEXTURE_2D );
    // hormones
    glColor4f( 1, 1, 1, mOpacity );
    gl::draw(mSensationsTexture, Vec2f( x - 300.0f, posY));
    
    glDisable( GL_TEXTURE_2D );

    
  float ex = getWindowWidth() - mTexture.getWidth();
  float ey = getWindowHeight() - mTexture.getHeight();
  
  for( int n = 0; n < 4; n++){
    
  float tempPos = mEmotions[n];
    float tempY;
    
    if(n == 0 ){
      gl::color(0.9f, 0.1f, 0.1f);
      tempY = ey-70.0f;
      gl::drawString("Happiness", Vec2f(ex, tempY));
      tempY = tempY + 14.0f;
      Rectf rect = Rectf(ex, tempY, ex + (tempPos * 100.0f),  tempY -2.0f);
      gl::drawSolidRect(rect);
    }
    if(n == 1 ){
      gl::color(0.1f, 0.9f, 0.1f);
      tempY = ey-50.0f;
      gl::drawString("Sadness", Vec2f(ex,tempY));
            tempY = tempY + 14.0f;
      Rectf rect = Rectf(ex, tempY, ex + (tempPos * 100.0f),   tempY -2.0f);
      gl::drawSolidRect(rect);
    }
    if(n == 2 ){
      gl::color(0.1f, 0.1f, 0.9f);
      tempY = ey-30.0f;
      gl::drawString("Fear", Vec2f(ex,tempY));
            tempY = tempY + 14.0f;
      Rectf rect = Rectf(ex, tempY, ex+ (tempPos * 100.0f),   tempY -2.0f);
      gl::drawSolidRect(rect);
    }
    if(n == 3 ){
      gl::color(0.6f, 0.6f, 0.1f);
      tempY = ey-10.0f;
      gl::drawString("Anger", Vec2f(ex,tempY));
            tempY = tempY + 14.0f;
      Rectf rect = Rectf(ex, tempY, ex+ (tempPos * 100.0f),   tempY -2.0f);
      gl::drawSolidRect(rect);
    }
    
      }
  }
  
  gl::enableDepthWrite( false );

}

float Model::feeling_intensity( int n ){ // Ifn
  float feeling_intensity;

  feeling_intensity = ( hormone_coefficient * mHormones[n] ) + getSensation(n);
  
  feeling_intensity = constrain(feeling_intensity, 0.0f, 1.0f);
  
  mFeelings[n] = feeling_intensity;
  return feeling_intensity;
}

float Model::getSensation(int n){
  float temp = mSensations[n];
  if (temp > 0.5f){
    return 1.0f;
  } else {
    return 0.0f;
  }
  return temp;
}


void Model::calculate_emotions(){
  for(int emotion = 0; emotion < 4; emotion++){
    float sigma, intensity;
    
    sigma = emotional_intensity( emotion );
    intensity = mCoeficient[mBias][emotion] + sigma;
    intensity = constrain( intensity, 0.0f, 1.0f );
    
    mEmotions[emotion] = intensity;

  }
}

float Model::emotional_intensity( int n ){
  float sigma = 0;
  for( int feeling = 0; feeling < 10; feeling++){
    
    float temp1 = mCoeficient[feeling][n];
    float temp2 = mFeelings[feeling];
    sigma += temp1 * temp2;
    
  }
  
  return sigma;
}

void Model::calculate_influence(){
  //cout << "Sensations[";
  
  //for( int sensation : mSensations ){
  for( int emotion = 0; emotion < 10; emotion++){
    //cout << "\t" << mSensations[sensation] << " | ";
    emotional_influence(emotion);
  }
  //cout << "\n";
}


float Model::emotional_influence( int n ){
  float influence = 0;
  for(int emotion = 0; emotion < 4; emotion++){
    
    if( mEmotions[emotion] > emotion_activation_threshold ){
      influence +=  mCoeficient[n][emotion] * mEmotions[emotion];
    }
    
  }
  
  mEmotionalInfluence[n] = influence;
  return influence;
  
}

void Model::calculate_hormones(){
  //cout << "Hormones [";
  for( int hormones = 0; hormones < 10; hormones++){
    //   cout << "\t" << hormone_influence(hormones) << "|";
    hormone_influence(hormones);
  }
  //cout << "\n";
}


float Model::hormone_influence( int num ){
  float influence;

  float HGain = hormone_gain(num);
  influence = HGain * mHormones[num]
  + ( 1 - HGain ) * mEmotionalInfluence[num];
  
  mHormones[num] = influence;
  return influence;
}

float Model::hormone_gain( int n ){
  if( abs(mEmotions[n]) > abs(mHormones[n]) ){ //?
    return hormone_attack_gain;
  } else {
    return hormone_decay_gain;
  }
  
}

void Model::calculate_feelings(){
  //cout << "Feelings [";
  for( int feeling = 0; feeling < 10; feeling++){
    //cout << "\t" << feeling_intensity( feeling ) << "|";
    //float temp = feeling_intensity(feeling);
    feeling_intensity(feeling);
    //cout << "\t" << temp << "|";
  }
  //cout << "\n";
  
}


void Model::calculate_dominant(){
  int dominant = 5;
  int dominantValue = 0;
  
  for(int emotion = 0; emotion < 4; emotion++){
    // If emotion value is above selection threshold
    if( mEmotions[emotion] > emotion_selection_threshold ){
      // If emotion is greater than the current dominant emotion
      if( mEmotions[emotion] > dominantValue){
        dominant = emotion;
        dominantValue = mEmotions[emotion];
      }
    }
  }

  switch( dominant ){
    case 0: dominantEmotion = HAPPINESS; break;
    case 1: dominantEmotion = SADDNESS; break;
    case 2: dominantEmotion = FEAR; break;
    case 3: dominantEmotion = ANGER; break;
    default: dominantEmotion = NONE; break;
  }

}


void Model::setSensation( int sensation, float value ){
  // Update sensations from raw sensor input
  float temp = mSensations[sensation];
  temp += value;
  temp = constrain(temp, 0.0f, 1.0f);
  mSensations[sensation] = temp;

}


string Model::get_sensations(){
  string toString = "Sensations :: \n";
  
  //for( int sensation : mSensations ){
  for( int sensation = 0; sensation < 10; sensation++){
    toString += boost::lexical_cast<string>( (double)mSensations[sensation] ) + "\n";

  }
  return toString;
}


string Model::get_feelings(){
  string toString;
  
  for( int n = 0; n < 10; n++){
    toString += boost::lexical_cast<string>( mFeelings[n] ) + " | ";
  }
  return toString + " :: Feelings";
}

string Model::get_hormones(){
  string toString;
  
  for( int n = 0; n < 10; n++){
    toString += boost::lexical_cast<string>( mHormones[n] ) + " | ";
  }
  return toString + " :: Hormones";
}


string Model::get_emotions(){
  string toString;
  
  for( int n = 0; n < 4; n++){
    toString += boost::lexical_cast<string>( mEmotions[n] ) + " | ";
  }
  return toString + " :: Emotions";
}

float Model::sensation_intensity( int emotion ){

  float sigma = 0;
  for( int sensation = 0; sensation < 10; sensation++){
      
    float temp1 = mCoeficient[sensation][emotion];
    float temp2 = mSensations[sensation];
    sigma += temp1 * temp2;
      
  }
  
  return sigma + mCoeficient[mBias][emotion]; // sigma + Be

}

void Model::calculate_emotion_based_reinforcement(){
  float R, e;

  // Ren = {
  // if foreach emotion, Ien < Iths; Ren = 0;
  // e.g. if no dominant emotion, return 0;
  if( dominantEmotion == NONE ){ // no dominant emotion
    R = 0;
  } else { // Ien Sign(e) where e = arg max( all Ien )
    e = mEmotions[behaviouralAction]; // get value of dominant emotion
  }

  // sign(e)
  if( e > 0 ){ e = 1; } else { e = 0; }
  
  // for each emotion
  for(int emotion = 0; emotion < 4; emotion++){

  R = e * mEmotions[emotion];
    mEmotionReinforcement[emotion] = R; // set emotional reinformement for later
  }

}

void Model::calculate_sensation_based_reinforcement(){
  float R, e;
  
  // Ren = {
  // if foreach emotion, Ien < Iths; Ren = 0;
  // e.g. if no dominant emotion, return 0;
  if( dominantEmotion == NONE ){ // no dominant emotion
    R = 0;
  } else { // Ien Sign(e) where e = arg max( Be + sigma F (Cef * Sfn)
    e = sensation_intensity(dominantEmotion); // get value of dominant emotion
  }
  
  // sign(e)
  if( e > 0 ){ e = 1; } else { e = 0; }
  
  // for each emotion
  for(int emotion = 0; emotion < 4; emotion++){
    
    R = e * sensation_intensity(emotion);
    mEmotionReinforcement[emotion] = R; // set emotional reinformement for later
  }
  
}

float Model::getEmotion(_entityEmotion emotion){
  switch( emotion ){
    case HAPPINESS:
      return mHappiness;
      break;
    case SADDNESS:
      return mSadness;
      break;
    case FEAR:
      return mFear;
      break;
    case ANGER:
      return mAnger;
      break;
  }
}

float Model::getFeeling(int n){
  return mFeelings[n];
}

void Model::select_behaviour(){
  // Select behaviour based on Boltzmann-Gibbs distribution
  float sigma;
  
  // calulate sum of all probabilities
  for(int action = 0; action < amountOfActions; action++ ){
    // find probability {
    sigma += probability_of_behaviour_selection(action);
  }
  
  int maxProbabilityIndex = -1;
  float maxValue = 0;
  // for each action, divide its probability by the sigma of all
  for(int action = 0; action < amountOfActions; action++ ){
    float temp = probability_of_behaviour_selection(action);
    float probability = temp / sigma;
    mActionSelection[action] = probability; // ? NO
    
    if( mActionSelection[action] > maxValue ){
      maxValue = mActionSelection[action];
      maxProbabilityIndex = action;
    }
  }
  
  // return behaviour with maxProbability
  // behaviour = maxProbability;
  behaviouralAction = maxProbabilityIndex;
  
}

float Model::probability_of_behaviour_selection( int a ){
  float T = 0.1;
  float Q;
  
  switch( a ){
    case 0:
        Q = mNeuralActionFWRD->getOutput();
      break;
    case 1:
        Q = mNeuralActionBWRD->getOutput();
      break;
    case 2:
        Q = mNeuralActionXPLR->getOutput();
      break;
    case 3:
        Q = mNeuralActionDONT->getOutput();
      break;
  }
  
  
  return Q / T;
}

int Model::max_behaviour(){

  int maxIndex = -1;
  float maxValue = -1;
  
  // for each action, divide its probability by the sigma of all
  for(int action = 0; action < amountOfActions; action++ ){
    float temp = get_neural_output(action);
    
    if( temp > maxValue ){
      maxValue = mActionSelection[action];
      maxIndex = action;
    }
  }

  return maxIndex;
}

float Model::get_neural_output( int a ){
  float output;
  
  switch( a ){
    case 0:
      output = mNeuralActionFWRD->getOutput();
      break;
    case 1:
      output = mNeuralActionBWRD->getOutput();
      break;
    case 2:
      output = mNeuralActionXPLR->getOutput();
      break;
    case 3:
      output = mNeuralActionDONT->getOutput();
      break;
  }
  
  return output;
}

_entityEmotion Model::getDominantEmotion(){
  return dominantEmotion;
}

void Model::info(){
  drawInfo = !drawInfo;
}

void Model::learn(){
  // Rn + \v max{ Qn (Sn, k) | k E behaviours }
  float T = 0;
  float V = 0.9f;

  T = mEmotionReinforcement[ behaviouralAction ] + ( V * max_behaviour() );
  
  // current behaviours neural network -> learn()
  switch( behaviouralAction ){
    case 0:
      mNeuralActionFWRD->learn(T);
      break;
    case 1:
      mNeuralActionBWRD->learn(T);
      break;
    case 2:
      mNeuralActionXPLR->learn(T);
      break;
    case 3:
      mNeuralActionDONT->learn(T);
      break;
  }

}
