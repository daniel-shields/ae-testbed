//
//  MayaOrthoCamUI.cpp
//  AETestbed
//
//  Created by Daniel Shields on 2014/02/26.
//
//  Adaption of Cinder / include / cinder / MayaCamUI.h
//  for an Orthographic style Camera
//

#include "MayaOrthoCamUI.h"

void MayaOrthoCamUI::centerCamera(){
    mCurrentCam.lookAt( mEye, mCenter, mUp );
  
    float deltaY = 0.0f;
    float deltaX = 0.0f;
    
    Vec3f mW { 0, 0, 1 };
    Vec3f mU = Vec3f::yAxis().cross( mW ).normalized();
    Vec3f mV = mW.cross( mU ).normalized();
    
    mCurrentCam.setEyePoint( mEye + mU * deltaX + mV * deltaY );
}

void MayaOrthoCamUI::focusZoom( float focusDistance ){
  mEye = Vec3f{ 0.0f, 0.0f, focusDistance };

}

void MayaOrthoCamUI::focusCamera( Vec2f focusPoint ){
  
  mCurrentCam.lookAt( mEye, mCenter, mUp );

  float deltaY = focusPoint.y;
  float deltaX = focusPoint.x;
  
  Vec3f mW { 0, 0, 1 };
  Vec3f mU = Vec3f::yAxis().cross( mW ).normalized();
  Vec3f mV = mW.cross( mU ).normalized();
  
  mCurrentCam.setEyePoint( mEye + mU * deltaX + mV * deltaY );
}

void MayaOrthoCamUI::zoomCamera( const Vec2i &mousePos ){
    int mouseDelta = ( mousePos.x - mInitialMousePos.x ) + ( mousePos.y - mInitialMousePos.y );
    float newCOI = powf( 2.71828183f, -mouseDelta / 500.0f ) * mInitialCam.getCenterOfInterest();
    Vec3f oldTarget = mInitialCam.getCenterOfInterestPoint();
    Vec3f newEye = oldTarget - mInitialCam.getViewDirection() * newCOI;
    mCurrentCam.setEyePoint( newEye );
    mCurrentCam.setCenterOfInterest( newCOI );
    
    mInitialCam = mCurrentCam;
    mInitialMousePos = mousePos;
    mMouseDepth = newEye.z;
  
    // type two
  
}

void MayaOrthoCamUI::panCamera( const Vec2i &mousePos ){
    // Currently the pan camera action jumps around a bit
    // the mouse has to be correctly mapped to the current screen co-ordinates
    // same reason why adding a new box does not happen under the mouse curser
    
  float deltaX = ( mousePos.x - mInitialMousePos.x ) / 1000.0f * mInitialCam.getCenterOfInterest();
  float deltaY = ( mousePos.y - mInitialMousePos.y ) / 1000.0f * mInitialCam.getCenterOfInterest();
    Vec3f mW = mInitialCam.getViewDirection().normalized();
    Vec3f mU = Vec3f::yAxis().cross( mW ).normalized();
    Vec3f mV = mW.cross( mU ).normalized();
    mCurrentCam.setEyePoint( mInitialCam.getEyePoint() + mU * deltaX + mV * deltaY );
    
}

void MayaOrthoCamUI::aspectRation( float ratio ){
    mCurrentCam.setPerspective(75.0f, ratio, 5.0f, 5000.0f );
    setCurrentCam(mCurrentCam);
}

void MayaOrthoCamUI::mouseDown( const Vec2i &mousePos ){
    mInitialCam = mCurrentCam;
    mInitialMousePos = mousePos;

}

const Vec2f MayaOrthoCamUI::mouseAdjust( const Vec2i &mousePos ){
    
    // Get the center of the screen
    Vec2f screenCenter = getWindowCenter();
    // Get the center of the box2d world
    Vec2f worldCenter { 2000, 20000 };

    // To convert betwen the screen and world,
    // take the distance from the center of the screen,
    // and apply the distance at a scale to the world
    
    // Subtract the center from the mouse position
    Vec2f adjustPos = mousePos - screenCenter;

    // get the scale of 1 to -1 by dividing the distance by the screen size
    float mouseX = ( adjustPos.x / getWindowWidth() );
    float mouseY = ( adjustPos.y / getWindowHeight() );

    // multiple by the world size to scale back up
    float scaledPosX = mouseX * 2000;
    float scaledPosY = mouseY * 2000;

    // add the values to the world center
    Vec2f scaledPos {scaledPosX, scaledPosY};
    scaledPos = scaledPos + worldCenter;

    return Vec2f{ scaledPos.x, scaledPos.y };
}

void MayaOrthoCamUI::mouseUp( const Vec2i &mousePos ){
    mInitialCam = mCurrentCam;
    mInitialMousePos = mousePos;
}

Vec3f MayaOrthoCamUI::screenToWorld(Vec2f mousePosition, float depth){

  float u = ((float) mousePosition.x ) / getWindowWidth();
  float v = ((float) (getWindowHeight() - mousePosition.y)) / getWindowHeight();
  Ray ray = mCurrentCam.generateRay( u, v, getWindowAspectRatio() );
  return ray.calcPosition( depth );

}

void MayaOrthoCamUI::update(){
  
  mModelView = gl::getModelView();
  mProjection = gl::getProjection();
  mViewport = gl::getViewport();
  
  // type two
  mWindowSize = Rectf( 0.0f, 0.0f, getWindowWidth(), getWindowHeight() );
  
}

/**
 * Code from bantherewind
 * https://forum.libcinder.org/topic/converting-the-mouse-position-to-3d-world-cordinates
 *
 */
Vec2f MayaOrthoCamUI::screenToWorldConversion( const Vec2f &point ){
  Vec3f point3f = Vec3f( (float)point.x, mWindowSize.getHeight() * 0.5f -
                        (float)point.y, 0.0f);
  Vec3f nearPlane = unprojectConversion(point3f);
  Vec3f farPlane = unprojectConversion(Vec3f( point3f.x, point3f.y, 1.0f ));
  
  // calc x, y and return
  float theta = (0.0f - nearPlane.z) / (farPlane.z - nearPlane.z);
  
  return Vec2f(
    nearPlane.x + theta * (farPlane.x - nearPlane.x),
    nearPlane.y + theta * (farPlane.y - nearPlane.y)
  );
}

Vec3f MayaOrthoCamUI::unprojectConversion( const Vec3f &point ){
  Matrix44f mInvMVP = mProjection * mModelView;
  mInvMVP.invert();
  
  Vec4f pointNormal;
  pointNormal.x = (point.x - mViewport.getX1()) / mViewport.getWidth() * 2.0f - 1.0f;
  pointNormal.y = (point.y - mViewport.getY1()) / mViewport.getHeight() * 2.0f;
  pointNormal.z = 2.0f * point.z - 1.0f;
  pointNormal.w = 1.0f;
  
  Vec4f pointCoord = mInvMVP * pointNormal;
  if( pointCoord.w != 0.0f )
    pointCoord.w = 1.0f / pointCoord.w;
  
  return Vec3f(
    pointCoord.x * pointCoord.w,
    pointCoord.y * pointCoord.w,
    pointCoord.z * pointCoord.w
  );
}
