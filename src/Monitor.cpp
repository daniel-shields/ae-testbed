//
//  Monitor.cpp
//  AETestbed
//
//  Created by Daniel Shields on 2014/03/24.
//
//

#include "Monitor.h"

Monitor::Monitor(){
  mGui = new Pretzel::PretzelGui("Monitor");
  mGui->setSize( Vec2i(150.0f, 200.0f) );
  mGui->setPos( Vec2i(250.0f, 10.0f) );
  mGui->minimize();
  
}

Monitor::~Monitor(){
  
}

void Monitor::addDependancyMonitor(float (&array)[11][4]){

  mHappinessGui = new Pretzel::PretzelGui("Happiness");
  mHappinessGui->setSize( Vec2i(150.0f, 400.0f) );
  mHappinessGui->setPos( Vec2i( 400.0f, 10.0f ) );
  
  mSadnessGui = new Pretzel::PretzelGui("Sadness");
  mSadnessGui->setSize( Vec2i(150.0f, 400.0f) );
  mSadnessGui->setPos( Vec2i( 550.0f, 10.0f ) );
  
  mFearGui = new Pretzel::PretzelGui("Fear");
  mFearGui->setSize( Vec2i(150.0f, 400.0f) );
  mFearGui->setPos( Vec2i( 700.0f, 10.0f ) );
  
  mAngerGui = new Pretzel::PretzelGui("Anger");
  mAngerGui->setSize( Vec2i(150.0f, 400.0f) );
  mAngerGui->setPos( Vec2i( 850.0f, 10.0f ) );
  
  for( int pos = 0; pos < 11; pos++){
    mHappinessGui->addSlider(names[pos], &array[pos][0], -1.0f, 1.0f);
    mSadnessGui->addSlider(names[pos], &array[pos][1], -1.0f, 1.0f);
    mFearGui->addSlider(names[pos], &array[pos][2], -1.0f, 1.0f);
    mAngerGui->addSlider(names[pos], &array[pos][3], -1.0f, 1.0f);
  }
  
  if( true ){
    mHappinessGui->minimize();
    mSadnessGui->minimize();
    mFearGui->minimize();
    mAngerGui->minimize();
  }

}

void Monitor::addMonitor(string text){
  mGui->addLabel(text);
}

void Monitor::update(){
  
}

void Monitor::toggle(){
  if( mState ){
    // toggle state
    mState = ! mState;
  }
}

void Monitor::draw(){
    mGui->draw();
    mHappinessGui->draw();
    mSadnessGui->draw();
    mFearGui->draw();
    mAngerGui->draw();
}