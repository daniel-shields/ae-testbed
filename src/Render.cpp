#include "Render.h"

Render::Render(){
	updateFlags();
}

Render::~Render(){
	
}

void Render::updateFlags(){
	SetFlags(
           ( drawShape * e_shapeBit ) |
           ( drawJoint * e_jointBit ) |
           ( drawAABB * e_aabbBit ) |
           ( drawPairs * e_pairBit ) |
           ( drawCenterOfMass * e_centerOfMassBit ) );
}


void Render::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color){
  Path2d path;
  path.moveTo( vertices[0].x, vertices[0].y );
  for( int i = 1; i < vertexCount; ++i )
  {
    path.lineTo( vertices[i].x, vertices[i].y );
  }
	
  gl::color( color.r, color.g, color.b );
  gl::draw( path );
}

void Render::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color){
  Path2d path;
  path.moveTo( vertices[0].x, vertices[0].y );
  for( int i = 1; i < vertexCount; ++i ){
    path.lineTo( vertices[i].x, vertices[i].y );
  }
	
  gl::enableAlphaBlending();
  gl::color( color.r, color.g, color.b, 0.5f );
  gl::drawSolid( path );
  gl::disableAlphaBlending();
  gl::draw( path );
}

void Render::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color){
  gl::color( color.r, color.g, color.b );
  gl::drawStrokedCircle( Vec2f{center.x, center.y}, radius );
}

void Render::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color){
  gl::enableAlphaBlending();
  gl::color( color.r, color.g, color.b, 0.5f );
  gl::drawSolidCircle( Vec2f{ center.x, center.y }, radius, 16 );
  gl::disableAlphaBlending();
  gl::drawStrokedCircle( Vec2f{center.x, center.y}, radius, 16 );
}

void Render::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color){
  gl::color(color.r, color.g, color.b);
  gl::drawLine( Vec2f{ p1.x, p1.y }, Vec2f{ p2.x, p2.y } );
}

void Render::DrawTransform(const b2Transform& xf){
	b2Vec2 p1 = xf.p, p2;
	const float32 k_axisScale = 0.4f;
	
	gl::color(1.0f, 0.0f, 0.0f);
	p2 = p1 + k_axisScale * xf.q.GetXAxis();
  gl::drawLine( Vec2f{ p1.x, p1.y }, Vec2f{ p2.x, p2.y } );
	
	gl::color(0.0f, 1.0f, 0.0f);
	p2 = p1 + k_axisScale * xf.q.GetYAxis();
  gl::drawLine( Vec2f{ p1.x, p1.y }, Vec2f{ p2.x, p2.y } );
}

void Render::DrawPoint(const b2Vec2& p, float32 size, const b2Color& color){
  glLineWidth(size);
	gl::color(color.r, color.g, color.b);
  gl::drawLine( Vec2f{ p.x, p.y }, Vec2f{ p.x, p.y } );
	glLineWidth(1.0f);
}

void Render::DrawString(int x, int y, const char* string, ...){
	std::cout << "WARNING: " <<  __PRETTY_FUNCTION__ << " Not Yet Implemented" << std::endl;
}

void Render::DrawAABB(b2AABB* aabb, const b2Color& color){
	gl::color(color.r, color.g, color.b);
  gl::drawStrokedRect( Rectf{ aabb->upperBound.x, aabb->upperBound.y, aabb->lowerBound.x, aabb->lowerBound.y }  );
}

