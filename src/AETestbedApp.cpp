#include "cinder/app/AppNative.h"
#include "cinder/gl/gl.h"
#include "cinder/params/Params.h"
#include "cinder/Camera.h"

#include "TestbedListener.h"
#include "ContactListener.h"
#include "MayaOrthoCamUI.h"
#include "BoundaryElement.h"
#include "Render.h"
#include "Monitor.h"
#include "AgentFactory.h"
#include "Agent.h"

#include <Box2D/Box2D.h>

#include "PretzelGui.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class AETestbedApp : public AppNative {
public:
  void prepareSettings( Settings *settings );
  void setup();
  void mouseDown( MouseEvent  event );
  void mouseDrag( MouseEvent  event );
  void mouseUp(   MouseEvent  event );
  void keyDown(   KeyEvent    event );
  void update();
  void draw();
  
  void reset();
  void pause();
  void load();
  void save();
  void spawn();
  
  void mouseDownListener( MouseEvent event );
  void mouseUpListerner( MouseEvent event );
  
  params::InterfaceGlRef  mParams;
  Pretzel::PretzelGui   *mGui;

  MayaOrthoCamUI      mMayaCam;
  CameraPersp         mInitialCam;

  b2World             *mWorld;

  BoundaryElement     *mBoundary;

  Monitor *mMonitor;
  AgentFactory        *mAgentFactory;
  Agent               *agent;
  
  ContactListener     mContactListenerInstance;
  
  Vec3f     direction;
  Vec3f     location;
  Vec3f     mTarget;
  
  Render    mDebugRender;
  bool      mPause;
  
  // Query world
  b2AABB    aabb;
  b2Vec2    d;
  b2Body    *body;
  Agent     *mPreviousHero;
  Agent     *mHero;
  float		mRadius;
	float		mOpacity;
  float   mFocusDistance;
  int   mWorldSpeed;
  int mLockmHero;
  bool mFollow;
  bool mDebugDraw;
  bool mDrawState;
  bool mContinuousDraw;
  
  int mHerbivoreToSpawn;
  int mPredatorsToSpawn;
  
  int mVelocityIterations; // strength of velocity adjustment
  int mPositionIterations; // strength of position adjustment
  float mTimestep;
  
  std::string mLoadPath = "map/default-map.xml";
  Font mFont;
  
  // Drawing
  vector<b2Vec2*>    mVertices;
  b2Vec2 position;
  b2Vec2 mWorldSize;
  bool mEmotionalModel;
  bool mBoids;
};

void AETestbedApp::prepareSettings( Settings *settings ){
  // MAYACAM: BOX2D_WORLD / 2
  settings->setWindowSize( 1024, 768 );
  settings->setFrameRate( 60.0f );
  
  //settings->enablePowerManagement();
  //settings->enableHighDensityDisplay();
}

void AETestbedApp::setup(){
  // BOX2D
  b2Vec2 gravity( 0.0f, 0.0f );
  mWorld = new b2World( gravity );

  mVelocityIterations = 7; // strength of velocity adjustment
  mPositionIterations = 3; // strength of position adjustment
  mTimestep = 1 / 60.0f; // length of time to simulate in seconds e.g. 1/framerate
  //mWorld->SetAllowSleeping(true);
  //mWorld->SetContinuousPhysics(true);
  
  mFont = Font( "Arial", 12.0f );

  

  mFollow = false;
  mFocusDistance = 250.0f;
  mWorldSpeed = 10;
  mDebugDraw = false;
  mLockmHero = 10;
  mDrawState = false;
  mContinuousDraw = false;
  mEmotionalModel = true;
  mBoids = false;
  mHerbivoreToSpawn = 5;
  mPredatorsToSpawn = 5;
  
  // GUI
  mRadius = 25;
	mOpacity = 0.5;
  mGui = new Pretzel::PretzelGui("AETesbed");
  mGui->setSize( Vec2i(200.0f, 450.0f) );
  mGui->addSlider("Simulation Speed", &mWorldSpeed, 1, 15);
  mGui->addSlider("Box2d Velocity Iterations", &mVelocityIterations, 1, 10);
  mGui->addSlider("Box2d Position Iterations", &mPositionIterations, 1, 10);
  mGui->addToggle("Debug Draw", &mDebugDraw );
  mGui->addLabel("Create Agents");
  mGui->addSlider("Herbivores", &mHerbivoreToSpawn, 0, 40);
  mGui->addSlider("Predators", &mPredatorsToSpawn, 0, 40);
  mGui->addToggle("Boids", &mBoids);
  mGui->addToggle("Emotional Model", &mEmotionalModel);
  mGui->addButton("Spawn Agents", &AETestbedApp::spawn, this);
  mGui->addLabel("Camera Settings");
  mGui->addButton("Reset Camera", &AETestbedApp::reset, this);
  mGui->addToggle("Toggle Pause", &mPause);
  mGui->addToggle("Follow Agent", &mFollow);
  mGui->addSlider("FocusZoom", &mFocusDistance, 50.0f, 500.0f);
  // LISTENER
  mPreviousHero = NULL;
  
  // CAMERA
  mInitialCam.setPerspective(75.0f, getWindowAspectRatio(), 5.0f, 5000.0f );
  mMayaCam.setCurrentCam( mInitialCam );
  mMayaCam.centerCamera();
  
  // PARAMATER WINDOW
    // WORLD
  mParams = params::InterfaceGl::create("AETestbedApp", Vec2i(400, 200));
  mParams->setOptions("", "position='10 500'"); // BarName, Options
  mParams->addSeparator();
  
    // MAP
  mParams->addText("Load or Save a map:");
  mParams->addParam("Load path:", &mLoadPath );
  mParams->addButton("Load map from path", std::bind( &AETestbedApp::load, this ));
  mParams->addText("loaded", "label=`no world loaded yet.`" );
  mParams->addButton("Save map state", std::bind( &AETestbedApp::save, this ));
  mParams->addSeparator();
  
    // BOIDS
  mParams->addText( "'Alt' + mouseClick to add block" );
  mParams->addText( "'f' to toggle fullscreen" );
  mParams->addText( "'?' to toggle info panel" );
  mParams->addSeparator();
  
  // AGENT FACTORY
  mAgentFactory = new AgentFactory( *mWorld );
  
  // MONITOR
  mMonitor = new Monitor();
  
  // BOUNDARY ELEMENT
  mBoundary = new BoundaryElement( *mWorld, *mAgentFactory );
  
  // CONTACT LISTENER
  mWorld->SetContactListener(&mContactListenerInstance);

  // DEBUG DRAW
  mWorld->SetDebugDraw( &mDebugRender );
  
  mPause = false;

  // clear out the window with black
  gl::clear( Color( 0.25f, 0.25f, 0.25f ) );
  gl::enableDepthRead( true );
  gl::enableDepthWrite( true );
  gl::enableAlphaBlending();
}

void AETestbedApp::load(){

  // LOAD MAP
  mBoundary->loadMap( mLoadPath );
  mParams->setOptions("loaded", "label=`world loaded successfully.`");
  mWorldSize = mBoundary->getWorldSize();
  
}

void AETestbedApp::save(){
  mBoundary->saveMap();
}

void AETestbedApp::reset(){
  // reset camera
  mMayaCam.focusZoom();
  mMayaCam.centerCamera();
}

void AETestbedApp::pause(){
  // reset camera
  if (mPause == true ) { mPause = false; } else { mPause = true; }
}

void AETestbedApp::spawn(){
  for( int h = 0; h < mHerbivoreToSpawn; h++ ){
    
    b2Vec2 position = *new b2Vec2(
          randFloat(0, mWorldSize.x), randFloat(0, mWorldSize.x) );
    
    mAgentFactory->create( _HERBIVORE, position );
  }
  
  for( int p = 0; p < mPredatorsToSpawn; p++ ){
    
    b2Vec2 position = *new b2Vec2(
          randFloat(0, mWorldSize.x), randFloat(0, mWorldSize.x) );
    
    mAgentFactory->create( _PREDATOR, position );
  }
}

void AETestbedApp::mouseDown( MouseEvent event ){

  if( event.isAltDown() ){
    // add herbivore
    Vec2f temp = mMayaCam.screenToWorldConversion( event.getPos() );
    b2Vec2 position = *new b2Vec2( temp.x, temp.y );
    mLockmHero = 1000;
    mAgentFactory->create( _HERBIVORE, position );
    
  } else if( event.isControlDown() ){
    // add predator
    Vec2f temp = mMayaCam.screenToWorldConversion( event.getPos() );
    b2Vec2 position = *new b2Vec2( temp.x, temp.y );
    mLockmHero = 1000;
    mAgentFactory->create( _PREDATOR, position );
    
  } else if ( event.isRight() ){

    if ( mDrawState ){
      
      if( event.isShiftDown()) { // use shift to create the first node
        Vec2f temp = mMayaCam.screenToWorldConversion( event.getPos() );
        position = *new b2Vec2( temp.x, temp.y ); // first pos
         mVertices.push_back( new b2Vec2( temp.x, temp.y ) );
      } else {
        // push back point to b2Vec vector
        Vec2f temp = mMayaCam.screenToWorldConversion( event.getPos() );
        mVertices.push_back( new b2Vec2( temp.x, temp.y ) );
      }
    }
    
  } else {
    mMayaCam.mouseDown( event.getPos() );
  }
  
  mouseDownListener( event );

}

void AETestbedApp::mouseDrag( MouseEvent event ){

 mMayaCam.panCamera( event.getPos() );

 if (event.isShiftDown() ){ // mouse zoon
      mMayaCam.zoomCamera( event.getPos() );
  }
}

void AETestbedApp::mouseUp( MouseEvent event ){
    mMayaCam.mouseUp( event.getPos() );
}

void AETestbedApp::keyDown( KeyEvent event ){
  if( event.getChar() == 'f' ){
      app::setFullScreen( ! app::isFullScreen() );
      mMayaCam.aspectRation( getWindowAspectRatio() );
      mMayaCam.centerCamera();
      
  } else if( event.getChar() == '?') {
    if( mHero != NULL ){
      if( mHero ){      // safety check that hero exists
        mHero->toggleInfo();   // toggle info panel on hero

      }
    }
  } else if( event.getChar() == 'p' ){
    // reset camera
    pause();
  } else if( event.getChar() == 'r' ){
    reset();
  } else if( event.getChar() == 'd' ){
      mDrawState = true;
  } else if( event.getChar() == 'b' || event.getChar() == 'w' || event.getChar() == 't'){
    mDrawState = false;
    
      if( mVertices.size() > 2 && mVertices.size() < 8){ // is a valid shape

        if( event.getChar() == 'b'){
          mAgentFactory->create( _BOUNDARY, position, mVertices );
        }
        if( event.getChar() == 'w'){
          mAgentFactory->create( _WATER, position, mVertices );
        }
        if( event.getChar() == 't' ){
          for( int t = 0; t < mVertices.size(); t++){
            mAgentFactory->create(_FOOD, *mVertices[t] );
          }
        }
      }
      
      mVertices = {}; // clear vertices
      position = *new b2Vec2(0.0f, 0.0f);
  }
  
}


void AETestbedApp::mouseDownListener( MouseEvent event ){
 
  b2Vec2 pos = b2Vec2( event.getPos().x, event.getPos().y );
  
  Vec2f temp { pos.x, pos.y }; // change to float later?
  Vec2f temp2f = mMayaCam.screenToWorldConversion(temp);
  
  pos = b2Vec2{ temp2f.x, temp2f.y };
  
  b2Vec2 extendBounds {0.1f, 0.1f};
  aabb.lowerBound = pos - extendBounds;
  aabb.upperBound = pos + extendBounds;
  
  // querycallback
  QueryCallback callback( pos );
  mWorld->QueryAABB( &callback, aabb );
  
  if( callback.m_fixture && mLockmHero < 10){

    // Get agent on mouse query
    body = callback.m_fixture->GetBody();
    mHero = static_cast<Agent*>(body->GetUserData());
    
    if( mPreviousHero != NULL ){
      // if previous mHero already exists, unset mHero agent
      mPreviousHero->unsetHero();
      
    }
    
    if( mHero != NULL ){ // safety check
      // mHero->mmHero = true; // set agent as new mHero
      mPreviousHero = mHero;
      mHero->setHero();

    }
    
  }

}

void AETestbedApp::mouseUpListerner( MouseEvent event ){
  //
}

void AETestbedApp::update(){
  if( ! mPause ){
    for( int i = 0; i < mWorldSpeed; ++i ){
      mWorld->Step( mTimestep, mVelocityIterations, mPositionIterations );
    }
      
    mAgentFactory->update();
  }
  
  // find mHero pos
  if( mHero != NULL && mFollow == true){
    b2Vec2 follow = mHero->getBody()->GetPosition();
    mMayaCam.focusZoom( mFocusDistance );
    mMayaCam.focusCamera( Vec2f( follow.x, follow.y ) );
  }

  // update mayacam
  mMayaCam.update();
  
  mLockmHero = mLockmHero - 1;
  mLockmHero = constrain(mLockmHero, 0, 50);

}

void AETestbedApp::draw(){
  // clear out the window with black

  // gl::enableDepthRead( true );
  gl::enableDepthWrite( true );
  gl::clear( Color( 0.25f, 0.25f, 0.25f ) );
  // gl::enableAdditiveBlending();
  // gl::enableAlphaBlending();

  gl::setMatrices( mMayaCam.getCamera() );

  
  if( mDebugDraw ){

      gl::pushModelView();
      mWorld->DrawDebugData();
      gl::popModelView();
    
  } else {
      mAgentFactory->draw();
  }
  


  if( mDrawState ){
    gl::color( COLOUR_DRAW );
    Vec2f previous = Vec2f(position.x, position.y);
    for(int i = 0; i < mVertices.size(); ++i){
      Vec2f drawPosition = Vec2f(mVertices[i]->x, mVertices[i]->y);
      gl::drawSolidCircle( drawPosition, 10.0f);
      
      gl::drawLine(previous, drawPosition);
      previous = drawPosition;

    }
  }
  
  
  mBoundary->draw();
  mParams->draw();

  
  gl::setMatricesWindow( getWindowWidth(), getWindowHeight() );
    mGui->draw();
    mAgentFactory->drawGui();

  
  gl::setMatrices( mMayaCam.getCamera() );
  gl::enableDepthWrite( false );
  
}

CINDER_APP_NATIVE( AETestbedApp, RendererGl )
