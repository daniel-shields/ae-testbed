//
//  Particle.cpp
//  AETestbed
//
//  Created by Daniel Shields on 2014/02/19.
//
//

#include "Particle.h"

Particle::Particle(b2World* mSandbox, b2Vec2 &pos, float radius, uint16 categoryBits, uint16 maskBits ){
//
  
  mWorld = mSandbox;
  mRadius = radius;
  mPosition = pos;
  mAngle = 0.0f; // random?
  
  
  // CREATE BODY
  body = NULL; // will it be necessary later?
  bodyDef.type = b2_dynamicBody;
  bodyDef.position.Set(pos.x, pos.y);
  body = mWorld->CreateBody( &bodyDef );
  
  // DEFINE BODY SHAPE
  b2Vec2 vertices[6];
  vertices[0].Set(    0,    0);
  vertices[1].Set(    3,  2.5);
  vertices[2].Set(  2.8,  5.5);
  vertices[3].Set(    0,   10);
  vertices[4].Set( -2.8,  5.5);
  vertices[5].Set(   -3,  2.5);
  dynamicBox.Set( vertices, 6);
  
  // DEFINE BODY FIXTURE
  fixtureDef.shape = &dynamicBox;
  fixtureDef.density = 1.0f;
  fixtureDef.friction = 0.3f;
  fixtureDef.restitution = 0.5f; // bounce
  
  // DEFINE AGENT CATEGORY & COLLISION TYPE
  //if( type == _PREDATOR ){
  //fixtureDef.filter.categoryBits = ENEMY_ENTITY;
  //fixtureDef.filter.maskBits = FRIENDLY_TOWER | RADAR_SENSOR | FRIENDLY_ENTITY | ENEMY_ENTITY;
  //} else {
  fixtureDef.filter.categoryBits = categoryBits;
  fixtureDef.filter.maskBits = maskBits;
  //}
  
  // ADD FIXTURE TO WORLD
  //b2Fixture* fixture =
  body->CreateFixture( &fixtureDef );
  
  //if( type == _PREDATOR ){
  //! fixture->SetUserData( new PredatorEntity() );
  
  //} else if ( type == _FRIEND ){
  //! fixture->SetUserData( new FriendEntity() );
  //}
  
  
  jointDef.bodyA = body;
  jointDef.enableLimit = true;
  jointDef.lowerAngle = 0;
  jointDef.upperAngle = 0;
  jointDef.localAnchorA.SetZero();
  
  // Sensor  *mSensor = new Sensor( mWorld );
  // jointDef.bodyB = mSensor->body;
  // jointDef.localAnchorA.Set( 3, 3.0f );
  // mWorld->CreateJoint( &jointDef );
  
  // tower sensor
  b2CircleShape circleShape1;
  circleShape1.m_p.Set( 0.0f, 10.0f );
  circleShape1.m_radius = 80.0f;
  fixtureDef.shape = &circleShape1;
  fixtureDef.density = 0;
  fixtureDef.isSensor = true;
  fixtureDef.filter.categoryBits = RADAR_SENSOR;
  fixtureDef.filter.maskBits = FRIENDLY_ENTITY | ENEMY_ENTITY;
  //b2Fixture* fixture1 = body->CreateFixture(&circleShape1, 1);//shape, density
  //fixture1->SetUserData( new TireEntity() );
  body->CreateFixture(&fixtureDef);
  
  // radar sensor
  mCircleRadius = 100.0f;
  float rad = mCircleRadius;
  
  initialRotation = 1.0f; // rotate the radar to align with the body
  
  /** RADAR
   b2Vec2 verticesR[8];
   verticesR[0].Set( 0, 12.0f );
   
   for( int i = 0; i < 7; i++ ){
   float angle = i / 6.0 * 60 * 0.0174532925199432957f + initialRotation;
   verticesR[i+1].Set( rad * cosf(angle), rad * sinf(angle) );
   }
   
   b2PolygonShape  polygonShapeRadar;
   polygonShapeRadar.Set(verticesR, 8);
   fixtureDef.shape = &polygonShapeRadar;
   fixtureDef.density = 0;
   fixtureDef.isSensor = true;
   fixtureDef.filter.categoryBits = RADAR_SENSOR;
   fixtureDef.filter.maskBits = FRIENDLY_ENTITY | ENEMY_ENTITY;
   
   //b2Fixture *fixtureRadar = body->CreateFixture(&fixtureDef);
   body->CreateFixture(&fixtureDef);
   //! fixtureRadar->SetUserData( new SensorEntity(  ) );
   */
  
  
  /**
   float maxForwardSpeed = 300;
   float maxBackwardSpeed = -40;
   
   // float backTireMaxDriveForce = 400;
   // float frontTireMaxDriveForce = 400;
   // float backTireMaxLateralImpulse = 8.5;
   // float frontTireMaxLateralImpulse = 8.5;
   
   float backTireMaxDriveForce = 950;
   float frontTireMaxDriveForce = 400;
   float backTireMaxLateralImpulse = 9;
   float frontTireMaxLateralImpulse = 9;
   
   
   float tireRadius = 2.0f;
   
   //back left tire
   b2BodyDef bodyDefTire1;
   bodyDefTire1.type = b2_dynamicBody;
   body = mWorld->CreateBody(&bodyDefTire1);
   
   b2CircleShape circleShape1;
   circleShape1.m_p.Set( 0.5f, 1.25f );
   circleShape1.m_radius = tireRadius;
   //b2Fixture* fixture1 = body->CreateFixture(&circleShape1, 1);//shape, density
   //fixture1->SetUserData( new TireEntity() );
   
   jointDef.bodyB = body;
   jointDef.localAnchorA.Set( -5, -1.0f );
   mWorld->CreateJoint( &jointDef );
   
   //back right tire
   b2BodyDef bodyDefTire2;
   bodyDefTire2.type = b2_dynamicBody;
   body = mWorld->CreateBody(&bodyDefTire2);
   
   b2CircleShape circleShape2;
   circleShape2.m_p.Set( 0.5f, 1.25f );
   circleShape2.m_radius = tireRadius;
   //b2Fixture* fixture2 = body->CreateFixture(&circleShape2, 1);//shape, density
   //fixture2->SetUserData( new TireEntity() );
   
   jointDef.bodyB = body;
   jointDef.localAnchorA.Set( 5, -1.0f );
   mWorld->CreateJoint( &jointDef );
   
   //front left tire
   b2BodyDef bodyDefTire3;
   bodyDefTire3.type = b2_dynamicBody;
   body = mWorld->CreateBody(&bodyDefTire3);
   
   b2CircleShape circleShape3;
   circleShape3.m_p.Set( 0.5f, 1.25f );
   circleShape3.m_radius = tireRadius;
   //b2Fixture* fixture3 = body->CreateFixture(&circleShape3, 1);//shape, density
   //fixture3->SetUserData( new TireEntity() );
   
   jointDef.bodyB = body;
   jointDef.localAnchorA.Set( 5, 10.0f );
   mWorld->CreateJoint( &jointDef );
   
   //front right tire
   b2BodyDef bodyDefTire4;
   bodyDefTire4.type = b2_dynamicBody;
   body = mWorld->CreateBody(&bodyDefTire4);
   
   b2CircleShape circleShape4;
   circleShape4.m_p.Set( 0.5f, 1.25f );
   circleShape4.m_radius = tireRadius;
   //b2Fixture* fixture4 = body->CreateFixture(&circleShape4, 1);//shape, density
   //fixture4->SetUserData( new TireEntity() );
   
   jointDef.bodyB = body;
   jointDef.localAnchorA.Set( -5, 10.0f );
   mWorld->CreateJoint( &jointDef );
   */
  
  particleType = categoryBits;
  
  // DEFINE USER DATA
  mHero = false;
  mContact = false;
  body->SetUserData( this );
  
  
  std::cout << "created at: " << pos.x << ", " << pos.y << "\n";

}


Particle::~Particle(){
  // delete
//  for (int i = 0; i < m_tires.size(); i++)
//    delete m_tires[i];
}

void Particle::pullToCenter(){
  Vec2f center { 2000, BOX2D_WORLD_HEIGHT/2 };
  
  b2Vec2 pos = body->GetPosition();
  Vec2f dirToCenter = Vec2f{pos.x, pos.y} - center;
  b2Vec2 tempDirForceToCenter = b2Vec2 { dirToCenter.x, dirToCenter.y };
  
  // experiement force to pull to centre is too strong, so lowering it
  // may be the box2d world loop is x10, may turn that off instead?
  tempDirForceToCenter = b2Vec2 {tempDirForceToCenter.x / 500.0f, tempDirForceToCenter.y / 500.0f};
  // endexperiment
  
  float disToCenter = dirToCenter.length();
  float maxDistance = 300.0f;
  
  if( disToCenter > maxDistance ){
    dirToCenter.normalize();
    // float pullStrength = 0.001f;
    // apply force to pull particle to center
    // b2Vec2 tempDir = b2Vec2{ dirToCenter.x, dirToCenter.y };
    b2Vec2 force = - tempDirForceToCenter;
    body->ApplyForce( force, body->GetWorldCenter() );
    
  }

}


void Particle::update(){
//  for (b2ContactEdge* edge = body->GetContactList(); edge; edge = edge->next){
//   cout << "touching: " << (bool)edge->contact->IsTouching() << "\n";
//  }
  
//  mContact = false;
  //std::cout << "particle pos: " << body->GetPosition().x << "," << body->GetPosition().y << "\n";
  pullToCenter();
  
  if( mHero ){
    //std::cout << "contact hero\n";
  } else {
    // no longer the hero
    //std::cout << "remove params monitor\n";
  }

  // BOIDS
  for(int i=0; i < mVisible.size(); i++){
    Particle* particle = (Particle*)mVisible[i]->body->GetUserData();
    
    b2Vec2 travel = body->GetPosition() - particle->mPosition;
    float distanceSquared = travel.LengthSquared();
    float percent = ( mZoneRadius/distanceSquared );
    
    if( percent < mThreshold ){
      // SEPERATION
      travel.Normalize();
      travel *= percent;
      
      //experiement force is too strong, so lowering it
      //may be the box2d world loop is x10, may turn that off instead?
      travel = b2Vec2 {travel.x / 150.0f, travel.y / 150.0f};
      //endexperiment

      this->applyLinearImpulse( travel );
      particle->applyLinearImpulse( -travel );
      
    } else if( percent < mHighThreshold ) {
      // ALIGNMENT
      float threshDelta = mHighThreshold - mThreshold;
      float adjust = ( percent - mThreshold ) / threshDelta;
      float F = ( 1.0 - ( cos(adjust * (M_PI * 2.0f)) * 0.5f + 0.5 ) * 0.05f );
      
      b2Vec2 changeFirst = b2Vec2{ particle->mLinearVelocity.x * F, particle->mLinearVelocity.y * F };
      b2Vec2 changeSecond = b2Vec2{ body->GetLinearVelocity().x * F, body->GetLinearVelocity().y * F };

      //experiement force is too strong, so lowering it
      //may be the box2d world loop is x10, may turn that off instead?
      // changeFirst = b2Vec2 {changeFirst.x * 10.0f, changeFirst.y * 10.0f};
      // changeSecond = b2Vec2 {changeSecond.x * 10.0f, changeSecond.y * 10.0f};
      //endexperiment
      
      this->applyLinearImpulse(changeFirst);
      particle->applyLinearImpulse(changeSecond);
      
    } else {
      // COHESION
      float threshDelta = 1.0f - mThreshold;
      float adjust = (percent - mThreshold) / threshDelta;
      
      float F = ( 1.0 - ( cos(adjust * (M_PI * 2.0f)) * 0.5f + 0.5 ) * 0.05f );
      
      travel.Normalize();
      travel *= F;
      
      this->applyLinearImpulse( -travel );
      particle->applyLinearImpulse( travel );
      
    }
    
  }
  
  
}


void Particle::applyLinearImpulse( b2Vec2 impulse ){
  body->ApplyLinearImpulse(impulse, body->GetWorldCenter() );
}

void Particle::setAsHero(){
  mHero = true;
  //mMonitor->addMonitor();
}


void Particle::draw(){
  
  if( body == NULL ) {    // safety
      return;
  }

  // convert angle radians to deg
  angle = mAngle * 180.0f/M_PI;

  if( mHero ){
    gl::color( ColorA( 0.97f, 0.10f, 0.58f, 0.8f ) );
  } else if( particleType == ENEMY_ENTITY ){
    gl::color( ColorA( 0.17f, 0.99f, 0.18f, 0.8f ) );
  } else if( particleType == RADAR_SENSOR ){
    gl::color( ColorA( 0.17f, 0.49f, 0.68f, 0.1f ) );
  } else if( particleType == TOWER_SENSOR ){
    gl::color( ColorA( 0.87f, 0.39f, 0.18f, 0.8f) );
  } else if( mContact ){
    gl::color( ColorA( 0.10f, 0.97f, 0.58f, 0.8f ) );
  } else {
    gl::color( ColorA( 0.99f, 0.84f, 0.46f, 0.8f ) );
  }


  
  b2Fixture* fixtures = body->GetFixtureList();
  while( fixtures != NULL ){
    
    switch( fixtures->GetType() ){
        
      case b2Shape::e_polygon: {
        if( particleType == RADAR_SENSOR ){
              gl::color( ColorA( 0.17f, 0.49f, 0.68f, 0.1f ) );
        b2PolygonShape* shape = (b2PolygonShape*) fixtures->GetShape();

        glPushMatrix();
        gl::enableAlphaBlending();
        
        gl::translate( Vec2f{ mPosition.x, mPosition.y } );
        gl::rotate( angle );
        glBegin(GL_POLYGON);

        for( int i = 0; i != shape->GetVertexCount(); ++i ){
          gl::vertex( shape->GetVertex(i).x, shape->GetVertex(i).y );
        }
     
        glEnd();
        glPopMatrix();
        

        } else { // other
          b2PolygonShape* shape = (b2PolygonShape*) fixtures->GetShape();
          
          glPushMatrix();
          gl::enableAlphaBlending();
          
          gl::translate( Vec2f{ mPosition.x, mPosition.y } );
          gl::rotate( angle );
          glBegin(GL_POLYGON);
          
          for( int i = 0; i != shape->GetVertexCount(); ++i ){
            gl::vertex( shape->GetVertex(i).x, shape->GetVertex(i).y );
          }
          
          glEnd();
          glPopMatrix();

        }

      }
      case b2Shape::e_circle: {
        if( particleType == TOWER_SENSOR ){
          // dont know why its drawing cirles for everything
          gl::enableAlphaBlending();
          gl::enableDepthRead();

           if (mContact){
             gl::color( ColorA( 0.47f, 0.66f, 0.10f, 0.3f ) ); }
          
          gl::drawSolidCircle(  Vec2f(mPosition.x, mPosition.y), mCircleRadius );
          
          gl::disableDepthRead();
          gl::disableAlphaBlending();
        }
      }
      default:
        break;
    }
    fixtures = fixtures->GetNext();
    
  }
  
  if (mContact){
    for(int i=0; i < mVisible.size(); i++){
      // draw line from this to enemy in vision
      //b2Vec2 enemyPos = mVisible[i]->body->GetPosition();
      
      Particle* particle = (Particle*)mVisible[i]->body->GetUserData();
      b2Vec2 enemyPos = particle->mPosition;
      
      Vec2f start {enemyPos.x, enemyPos.y};
      Vec2f end {mPosition.x, mPosition.y + 10.0f};
      
      gl::enableAdditiveBlending();
      gl::enableAlphaBlending();

      
      if (particle->particleType == FRIENDLY_ENTITY ){
        gl::color( Color( 0.39f, 0.99f, 0.99f ) );
      } else {
        gl::color( Color( 0.99f, 0.39f, 0.99f ) );
      }
      
      gl::drawLine(start, end);
      gl::disableAlphaBlending();
      
    }
  }

}

void Particle::visionAcquiredEnemy(Particle *enemy){
  //std::cout << "vision acquired: " << enemy  << "\n";
  mVisible.push_back( enemy );
  mContact = true;
}

void Particle::visionLostEnemy(Particle* enemy){
  //std::cout << "vision lost: " << enemy << "\n";
  mVisible.erase( std::find(mVisible.begin(), mVisible.end(), enemy ));
  
  if( mVisible.size() == 0){
    mContact = false;
  }
}
