//
//  ParticleController.cpp
//  AETestbed
//
//  Created by Daniel Shields on 2014/02/19.
//
//

#include "ParticleController.h"
#include <iostream>

ParticleController::ParticleController( b2World &mSandbox ){
    mWorld = &mSandbox;

}

ParticleController::~ParticleController(){
    // Clear particles from memory
    for( int i = 0; i < mParticles.size(); i++ ){
        delete mParticles[i];
    }
}

void ParticleController::addParticles( int num ){

    for(int i = 0; i < num; i++){
        float x = Rand::randFloat(0.0f, BOX2D_WORLD_WIDTH);
        float y = Rand::randFloat(0.0f, BOX2D_WORLD_HEIGHT);
        b2Vec2 pos = b2Vec2{ x, y };
        float radius = Rand::randFloat(BOX_SIZE, (BOX_SIZE*2));
        bool f = Rand::randBool();
      
        Particle tempParticle = *new Particle( mWorld, pos, radius, FRIENDLY_ENTITY, RADAR_SENSOR | ENEMY_ENTITY | FRIENDLY_ENTITY );
        mParticles.push_back( &tempParticle );
   
    
    }

}

void ParticleController::addParticles( Vec2f mousePos ){
  
  // adjust the mouse co-ordinates, the box2d world is 1800m / 1800px wide,
  // the camera is set to 2x distance, the screen height and width is 900x
  // this finds the percentage of the values according to the app window width / height
  // and multiplies it by the box2d world width and height
  
  // subtract distance from center of window
  // multiple distance from center to world
  float radius = Rand::randFloat(BOX_SIZE, (BOX_SIZE*1.5));

  b2Vec2 pos = b2Vec2{ mousePos.x, mousePos.y };

  bool f = Rand::randBool();
  
  if( f == true ){
  Particle tempParticle = *new Particle( mWorld, pos, radius, FRIENDLY_ENTITY, RADAR_SENSOR | ENEMY_ENTITY | FRIENDLY_ENTITY | BOUNDARY );
  mParticles.push_back( &tempParticle );
    cout << "friend created\n";
  } else {
    
    Particle tempParticle = *new Particle( mWorld, pos, radius, ENEMY_ENTITY, RADAR_SENSOR | ENEMY_ENTITY | FRIENDLY_ENTITY | BOUNDARY );
    mParticles.push_back( &tempParticle );
        cout << "enemy created\n";
  }
  
}

void ParticleController::update(){
    
    /** ====== USER DATA ======================= */
    /** Merge this section with the one below    */
    b2Body* userBodies = mWorld->GetBodyList();
    while (userBodies != NULL){
       
        Particle* part = static_cast<Particle*>( userBodies->GetUserData() );
        if( part != NULL ){

            part->mPosition = userBodies->GetPosition();
            part->mAngle = userBodies->GetAngle();
            part->mLinearVelocity = userBodies->GetLinearVelocity();

            // Update particle
            part->update();
          
        }
      
        userBodies = userBodies->GetNext();
    }
    
    /** ======================================== */
    /**
    b2Body* first = mWorld->GetBodyList();

    while( first ){

        b2Body* second = mWorld->GetBodyList();
        while( second ){
            
            b2Vec2 travel = first->GetPosition() - second->GetPosition();
            float distanceSquared = travel.LengthSquared();
            
            // Seperation force
            if( distanceSquared <= zoneRadius && (travel.x != 0) && (travel.y != 0 )){
                float percent = ( zoneRadius/distanceSquared );
              
            }
            
            second = second->GetNext();
        }

        first = first->GetNext();
    }
    */
    
}

void ParticleController::draw(){
  b2Body* userBodies = mWorld->GetBodyList();
  while (userBodies != NULL){
    
    Particle* part = static_cast<Particle*>( userBodies->GetUserData() );
    if( part != NULL ){
      part->draw();
    }
  

    userBodies = userBodies->GetNext();
  }

}


void ParticleController::debugDraw(){
  
	gl::color( Color( 0.99f, 0.84f, 0.46f ) );
  
  b2Body* bodies = mWorld->GetBodyList();
  while( bodies != NULL ){
    b2Vec2 pos;
    float32 angle;
    // UserData
    Particle* userDataParticle = static_cast<Particle*>( bodies->GetUserData() );
    if( userDataParticle != NULL ){
      pos = userDataParticle->mPosition;
      angle = userDataParticle->mAngle;
    } else {
      //pos = bodies->GetPosition();
      //angle = bodies->GetAngle();
    }
    
    // convert angle radians to deg.
    angle = angle * 180.0f/M_PI;
    
    glPushMatrix();
    gl::translate( Vec2f{ pos.x, pos.y } );
    gl::rotate( angle );
    
    b2Fixture* fixtures = bodies->GetFixtureList();
    while( fixtures != NULL ){
      
      switch( fixtures->GetType() ){
        case b2Shape::e_polygon: {
          
          b2PolygonShape* shape = (b2PolygonShape*) fixtures->GetShape();
          glBegin(GL_POLYGON);
          for( int i = 0; i != shape->GetVertexCount(); ++i ){
            gl::vertex( shape->GetVertex(i).x, shape->GetVertex(i).y );
          }
          glEnd();
        }

        default:
          break;
      }
      fixtures = fixtures->GetNext();
      
    }
    
    glPopMatrix();
    bodies = bodies->GetNext();
    
  }
  
}