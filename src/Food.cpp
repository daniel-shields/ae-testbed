//
//  Food.cpp
//  AETestbed
//
//  Created by Daniel Shields on 2014/04/15.
//
//

#include "Food.h"

Food::Food( b2World* mSandbox, b2Vec2 &position ){
  
  mWorld = mSandbox;
  
  // CREATE BODY
  body = NULL;
  bodyDef.type = b2_staticBody;
  bodyDef.position.Set(position.x, position.y);
  body = mWorld->CreateBody( &bodyDef );
  
  // DEFINE BODY SHAPE
  b2CircleShape circleShape;
  circleShape.m_p.Set(0.0f, 0.0f);
  mCircleRadius = 10.0f;
  circleShape.m_radius = mCircleRadius;
  fixtureDef.shape = &circleShape;
  fixtureDef.density = 1.0f;
  fixtureDef.isSensor = false;
  fixtureDef.filter.categoryBits = categoryBits;
  fixtureDef.filter.maskBits = maskBits;
  
  // ADD FIXTURE TO WORLD
  body->CreateFixture( &fixtureDef );
  
  // DEFINE USER DATA
  mHero = false;
  mContact = false;
  body->SetUserData( this );
  
  mPosition = body->GetPosition();
}


void Food::update(){
  
  mPosition = body->GetPosition();
  mAngle = body->GetAngle();

  mRadius = health/10;
  setHealth(0.0001);
}

void Food::setHealth(float value){
  health += value;

  health = constrain(health, 0.0f, 100.0f);
}

void Food::draw(){

  if( body == NULL ) {    // safety
    return;
  }
  
  // convert angle radians to deg
  float angle = mAngle * 180.0f/M_PI;
  gl::color( COLOUR_FOOD );
  
  // DRAW FIXTURES
  
  b2Fixture* fixtures = body->GetFixtureList();
  while( fixtures != NULL ){
    
    switch( fixtures->GetType() ){

      case b2Shape::e_circle: {
        
        gl::drawStrokedCircle( Vec2f(mPosition.x, mPosition.y), mCircleRadius );
        gl::drawSolidCircle( Vec2f(mPosition.x, mPosition.y), mRadius );
        
      }
      default:
        break;
    }
    fixtures = fixtures->GetNext();
    
  }
  

}

string Food::getXmlString(){
  return "food";
}

XmlTree Food::getXmlVertices(){
  XmlTree geometry("geometry", "");
  
  return geometry;
}
