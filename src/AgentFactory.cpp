//
//  AgentFactory.cpp
//  AETestbed
//
//  Created by Daniel Shields on 2014/04/15.
//
//

#include "AgentFactory.h"

AgentFactory::~AgentFactory(){
  for(int i = 0; i < mAgents.size(); ++i){
    mAgents.pop_back(); // remove each agent in the vector
  }
}

void AgentFactory::create( agentFixtureType type, b2Vec2 &pos,
                            vector<b2Vec2*> mVertices ){

  mAgents.push_back( getAgent( type, pos, mVertices ) );
}

void AgentFactory::update(){
  
  for(int i = 0; i < mAgents.size(); ++i){
    
    // if agent is dead
    if( mAgents[i]->getHealth()){ // remove agent
      mAgents.erase( std::find(mAgents.begin(), mAgents.end(), mAgents[i]) );
    } else {  // update agent
      mAgents[i]->update();
    }
    
  }
  
}

void AgentFactory::draw(){
  
  for(int i = 0; i < mAgents.size(); ++i){
    mAgents[i]->draw();
  }
  
}

void AgentFactory::drawGui(){
  
  for(int i = 0; i < mAgents.size(); ++i){
    mAgents[i]->drawGui();
  }
  
}

XmlTree AgentFactory::getXmlString(){
  
   XmlTree world("world", "");

  for(int i = 0; i < mAgents.size(); ++i){
    XmlTree element("element", "");
    element.setAttribute("type", mAgents[i]->getXmlString() );
    element.setAttribute("posx", mAgents[i]->getBody()->GetPosition().x );
    element.setAttribute("posy", mAgents[i]->getBody()->GetPosition().y );
    
    
    XmlTree geometry = mAgents[i]->getXmlVertices();
    XmlTree attributes("attributes", "");
    
    element.push_back(geometry);
    element.push_back(attributes);
    world.push_back(element);

  }
  
  return world;

}


