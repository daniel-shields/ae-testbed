//
//  ContactListener.cpp
//  AETestbed
//
//  Created by Daniel Shields on 2014/03/13.
//
//

#include "ContactListener.h"

void ContactListener::BeginContact( b2Contact *contact ){
  Agent* sensor;
  Agent* particle;
  
  if ( handleContact( contact, sensor, particle, true )){
    sensor->visionAcquiredEnemy( particle );
  }
}

void ContactListener::EndContact( b2Contact *contact ){
  Agent *sensor;
  Agent *particle;
  if( handleContact( contact, sensor, particle, false )){
    sensor->visionLostEnemy( particle );
  }
}

bool ContactListener::handleContact( b2Contact* contact, Agent*& sensor, Agent*& particle, bool state ){
  {
    
    b2Fixture *fixtureA = contact->GetFixtureA();
    b2Fixture *fixtureB = contact->GetFixtureB();
    
    bool sensorA = fixtureA->IsSensor();
    bool sensorB = fixtureB->IsSensor();
    
    if( ! (sensorA ^ sensorB) ){
      return false;
    }
    
    Agent *entityA = static_cast<Agent*>( fixtureA->GetBody()->GetUserData());
    Agent *entityB = static_cast<Agent*>( fixtureB->GetBody()->GetUserData());
    
    if( sensorA ){
      sensor = entityA;
      particle = entityB;
    } else {
      sensor = entityB;
      particle = entityA;
    }
    
    return true;
    
  }
}