//
//  NeuralNetwork.cpp
//  AETestbed
//
//  Created by Daniel Shields on 2014/05/07.
//
//

#include "NeuralNetwork.h"

NeuralNetwork::NeuralNetwork(){
  // cout << "new neural network\n";
  for( int i = 0; i < mHidden; i++ ){
    mNetwork[mHiddenLayer][i] = 0.0f; // a random value
    mHiddenWeights[i][0] = (float)Rand::randFloat(-1.0f, 1.0f ); // random
  }
  
  for(int i = 0; i < 10; i++ ){
    for(int j = 0; j < 10; j++){
      mInputWeights[i][j] = (float)Rand::randFloat(-0.3f, 0.3f);
    }
  }
  
  // set up the threshold bias, may be emotional bias?
  mNetwork[mInputLayer][mInputs] = -1.0f;
  
  // set up the one output neuron
  mNetwork[mOutput][0] = 0.0f;
  
  
}

NeuralNetwork::~NeuralNetwork(){
  // delete all array
}

void NeuralNetwork::input( float (&array)[10] ){
  for( int i = 0; i < mInputs; i++ ){
    mNetwork[mInputLayer][i] = array[i];
  }
}

void NeuralNetwork::fire(){
  
  for( int h = 0; h < mHidden; h++){
    mNetwork[mHiddenLayer][h] = 0.0f; // reset values
    for( int i = 0; i < mInputs; i++){

      mNetwork[mHiddenLayer][h] +=
        mNetwork[mInputLayer][i] * mInputWeights[h][i];

    }
    mNetwork[mHiddenLayer][h] = activationFunction( mNetwork[mHiddenLayer][h] );
  }
  
  // output neurons
  mNetwork[mOutputLayer][0] = 0.0f; // reset value
    
  for( int h = 0; h < mHidden; h++ ){
    mNetwork[mOutputLayer][0] +=
      mNetwork[mHiddenLayer][h] * mHiddenWeights[h][0];

  }
  
  mNetwork[mOutputLayer][0] = activationFunction( mNetwork[mOutputLayer][0] );
  // cout << "neural network output = " << mNetwork[mOutputLayer][0] << "\n";
}

void NeuralNetwork::learn( float T ){
  // back propogate with target value T
  float learning_rate = 0.3f;
  float momentum_const = 0.8f;
  
  // GET OUTPUT GRADIENT
  // calculate error gradient of output neuron
  // get output_value * (1 - output_value) * (desired_value - output_value)
  float Yk = mNetwork[mOutputLayer][0];
  float Dk = T;
  float gradient = Yk * (1 - Yk) * (Dk - Yk);
  
  // GET HIDDEN GRADIENT
  // for each hidden layer neuron
  // get sigma hidden->output weight * error gradient of output
  float sigma = 0.0f;
  
  for( int h = 0; h < mHidden; h++ ){
    sigma += mHiddenWeights[h][0];
  }
  
  // UPDATE HIDDEN WEIGHTS
  for( int j = 0; j < mHidden; j++){
    // Wjk = Wjk + delta(Wjk)
    // delta(Wjk) = learning_rate * hidden_neuron_valuej * gradient
    //float Wij = delta(mNetwork[mHiddenLayer][j]);
    //float momentum = momentum_const * Wij * (T-1);
    bool use_momentum = true;
    float momentum;
    if( use_momentum ){
      momentum = momentum_const * mNetwork[mHiddenLayer][j] * (T - 1);
    } else {
      momentum = 0.0f;
    }
    float delta1 = (learning_rate * mNetwork[mHiddenLayer][j] * gradient) + momentum;
    mHiddenWeights[j][0] = delta1;
    
    // AND UPDATE INPUT WEIGHTS
    for( int i = 0; i < mHidden; i++){
      float Yj = mNetwork[mHiddenLayer][j];
      float hidden_gradient = ( Yj * ( 1 - Yj ) ) * sigma * gradient;
      // update weights
      // Wij = Wij + delta(Wij)
      // delta(Wij) = learning_rate * input_neuron_valuei * hidden_gradient
      float inputs_momentum;
      float Ij = mNetwork[mInputLayer][i];
      if( use_momentum ){
        inputs_momentum = momentum_const * Ij * (T - 1);
      } else {
       inputs_momentum = 0.0f;
      }
      float delta2 = (learning_rate * Ij * hidden_gradient) + inputs_momentum;
      mInputWeights[j][i] = delta2;
    }
  }
  
  
}

float NeuralNetwork::getOutput(){
  return mNetwork[mOutput][0];
}

float NeuralNetwork::activationFunction( float x ){
  // Hyperbolic tanget activation function
  float e = M_E; // eulers constant
  float Bx = 0.25 * x; // B = 0.25
  
  float sin = 1 - powf(e, -2 * Bx);
  float cos = 1 + powf(e, -2 * Bx);

  float tanh = sin / cos;
  return tanh;
}


string NeuralNetwork::getInput(int n) {
  string toString;
  
  for( int i = 0; i < 10; i++){
    toString += boost::lexical_cast<string>( mNetwork[n][i] ) + " | ";
  }
  
  return toString;
}

string NeuralNetwork::getWeight(int n){
  string toString;
  
  if( n == 0 ){
    // mInputWeights
    for( int i = 0; i < 10; i++){
      toString += boost::lexical_cast<string>( mInputWeights[i][0] ) + " | ";
    }
  } else if (n == 1){
    // mHiddenWeights
    for( int i = 0; i < 10; i++){
      toString += boost::lexical_cast<string>( mHiddenWeights[i][0] ) + " | ";
    }
  }
  
  return toString;
}

