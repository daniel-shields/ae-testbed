//
//  Water.cpp
//  AETestbed
//
//  Created by Daniel Shields on 2014/05/13.
//
//

#include "Water.h"

Water::Water( b2World* mSandbox, b2Vec2 &position, vector<b2Vec2*> mVertices ){
  
  mWorld = mSandbox;
  
  // CREATE BODY
  body = NULL; // will it be necessary later?
  bodyDef.type = b2_staticBody;
  bodyDef.position.Set(position.x, position.y);
  body = mWorld->CreateBody( &bodyDef );
  
  // Vertices
  // move coords from vector to vertices array
  verticesCount = mVertices.size();
  //int verticesCount = 3;
  vertices = new b2Vec2[ verticesCount ];
  for (int i = 0; i < verticesCount; i++){
    vertices[ i ].Set( mVertices[i]->x, mVertices[i]->y );
  }

  // Define Body Shape
  b2PolygonShape polygonShape;
  polygonShape.Set(vertices, verticesCount); //pass array to the shape
  fixtureDef.shape = &polygonShape; //change the shape of the fixture
  //  body->CreateFixture(&fixtureDef); //add a fixture to the body
  
  // DEFINE BODY SHAPE
  //  b2CircleShape circleShape;
  //  circleShape.m_p.Set(0.0f, 0.0f);
  //  mCircleRadius = 10.0f;
  //  circleShape.m_radius = mCircleRadius;
  //  fixtureDef.shape = &circleShape;
  //  fixtureDef.density = 0;
  fixtureDef.restitution = 0;
  fixtureDef.friction = 0;
  fixtureDef.isSensor = false;
  fixtureDef.filter.categoryBits = categoryBits;
  fixtureDef.filter.maskBits = maskBits;

  
  // ADD FIXTURE TO WORLD
  body->CreateFixture( &fixtureDef );
  
  // DEFINE USER DATA
  mHero = false;
  mContact = false;
  body->SetUserData( this );

  mPosition = body->GetPosition();
  mAngle = body->GetAngle();
}


void Water::update(){
  //
}

void Water::setHealth(float value){
  //
}

void Water::draw(){
  
  if( body == NULL ) {    // safety
    return;
  }
  
  // convert angle radians to deg
  float angle = mAngle * 180.0f/M_PI;
  gl::color( COLOUR_WATER );
  
  // DRAW FIXTURES
  
  b2Fixture* fixtures = body->GetFixtureList();
  while( fixtures != NULL ){
    
    switch( fixtures->GetType() ){
        
      case b2Shape::e_polygon: {

        b2PolygonShape* shape = (b2PolygonShape*) fixtures->GetShape();
        
        glPushMatrix();

        gl::translate( Vec2f{ mPosition.x, mPosition.y } );
        gl::rotate( angle );
        glBegin(GL_POLYGON);
        
        for( int i = 0; i != shape->GetVertexCount(); ++i ){
          gl::vertex( shape->GetVertex(i).x, shape->GetVertex(i).y );
        }
        
        glEnd();

        glPopMatrix();

      }
      default:
        break;
    }
    fixtures = fixtures->GetNext();
    
  }
  
  
}

string Water::getXmlString(){
  return "water";
}

XmlTree Water::getXmlVertices(){
  XmlTree geometry("geometry", "");
  
  for( int i =0; i < verticesCount; i++){
    
    b2Vec2 temp = vertices[i];
    
    XmlTree coords("coords", "");
    coords.setAttribute("x", (int)temp.x );
    coords.setAttribute("y", (int)temp.y );
    geometry.push_back(coords);
    
  }
  
  return geometry;
}