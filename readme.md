#AE Testbed
An testbed for monitoring emotional models in an artificial life simulation.

This project aims to explore the use of emotional models on motivation and behavior in a population of agents in an artificial life simulation. The system will provide an artificial life sandbox in which the calculation and results of artificial emotion models can be observed in agents during the simulation, providing a real time observation of the models, and providing the ability for the simulation parameters to be modified. 

The software is being built with [libcinder](http://libcinder.org/download/)
 and [box2d](http://www.box2d.org/).

##Compile
To get the code to compile with XCode (currently 5.0.1), download cinder for mac [cinder_0.8.5_mac.zip](http://libcinder.org/download/)

If you open XCode and if you try compile the project it should warn you to update the project settings, with a Preprocessor Issue "'cinder/Cinder.h' file not found"

The project has build settings such as "Other Linker Flags Debug / Release" that include the variable "$(CINDER_PATH)" which will need to be updated in order for this to compile properly.

At the bottom of the build settings panel, there will be a section for User-Defined variables. Update the "CINDER_PATH" variable to the correct destination.

In my case I had to change it from "../../../cinder" into "../../cinder_0.8.5_mac", and my folder tree structure is as follows:

    - Downloads
        - Project
              - AETestbed
              - cinder_0.8.5_mac

With AETestbed and cinder in the same folder.
